const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.env.NODE_ENV !== "production";
const marked = require("marked");
const renderer = new marked.Renderer();

function srcPath(subdir) {
	return path.join(__dirname, "src", subdir);
}

module.exports = {
	entry: ["@babel/polyfill", "./src/index"],

	output: {
		path: path.join(__dirname, "/dist"),
		filename: "bundle.js",
		publicPath: "/"
	},

	resolve: {
		extensions: [".ts", ".tsx", ".mjs", ".js"],
		alias: {
			components: srcPath("components"),
			app: srcPath("app"),
			codegen: srcPath("codegen"),
			scss: srcPath("scss"),
			helper: srcPath("helper"),
			src: path.join(__dirname, "src")
		}
	},
	module: {
		rules: [
			{
				test: /\.(ts|js)x?$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.md$/,
				use: [
					{
						loader: "html-loader"
					},
					{
						loader: "markdown-loader",
						options: {
							pedantic: true,
							renderer
						}
					}
				]
			},

			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							hmr: process.env.NODE_ENV === "development"
						}
					},
					"css-loader",
					"sass-loader",
					"postcss-loader"
				]
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "./src/index.html"
		}),
		new MiniCssExtractPlugin({
			filename: devMode ? "[name].css" : "[name].[hash].css",
			chunkFilename: devMode ? "[id].css" : "[id].[hash].css"
		})
	],
	devServer: {
		contentBase: path.resolve(__dirname, "dist"),
		historyApiFallback: true,
		port: 3000,
		hot: true,
		stats: "minimal"
	}
};
