import * as React from "react";
import { Redirect, Route, RouteComponentProps, RouteProps } from "react-router-dom";
import { getAccessToken } from "helper";


type RouteComponent = React.StatelessComponent<RouteComponentProps<{}>> | React.ComponentClass<any>;


export const PrivateRoute: React.StatelessComponent<RouteProps> = ({ component, ...rest }) => {


	// console.log(localStorage.getItem(CognitoAccessToken))
	const renderFn = (Component?: RouteComponent) => (props: RouteProps) => {
		if (!Component) {
			return null;
		}

		if (getAccessToken() !== null) {
			return <Component {...props} />;
		}

		const redirectProps = {
			to: {
				pathname: "/login",
				state: { from: props.location }
			}
		};

		return <Redirect {...redirectProps} />;
	};

	return <Route {...rest} render={renderFn(component)} />;
};
