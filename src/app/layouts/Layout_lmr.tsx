import React from "react";

export interface InjectedProps {
	Left: React.ComponentType<React.HTMLAttributes<any>>;
	Middle: React.ComponentType<{ title: string } & React.HTMLAttributes<any>>;
	Right: React.ComponentType;
}

interface IProps {
	children(props: InjectedProps): JSX.Element;
}

class LayoutLMR extends React.PureComponent<IProps> {
	HeaderPane = (props?: any) => {
		return <div className="header">{props.children}</div>;
	};
	LeftPane = (props: any) => {
		return (
			<div className={`left `.concat(props.className)}>
				{props.children}
			</div>
		);
	};
	MiddlePane = (props?: any) => {
		return <div className={"middle ".concat(props.className)}>{props.children}</div>;
	};

	RightPane = (props?: any) => {
		return <div className={"middle ".concat(props.className)}>{props.children}</div>;
	};
	render() {
		return (
			<div className="app-lmr-view">
				{this.props.children({
					Left: this.LeftPane.bind(this),
					Middle: this.MiddlePane.bind(this),
					Right: this.RightPane.bind(this)
				})}
			</div>
		);
	}
}

export default LayoutLMR;
