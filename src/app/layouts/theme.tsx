export interface ITheme {
	main: any;
	button?: any;
	colors: any,
}

export const theme: ITheme = {
	main: { color: "blue" },
	button: { backgroundColor: "red" },
	colors: {
		borderColor: "rgba(0, 0, 0, 0.08)"
	}
};
