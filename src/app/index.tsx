import React from "react";
import "../scss/site.scss";
import _ from "lodash";
import { shortid } from "helper";

import RightMenu from "../components/RightMenu";
import Home from "./pages/Home";
import Projects from "./pages/Projects";
import Calendar from "./pages/Calendar";

import { ProjectTasks } from "./pages/Projects/Views/ProjectTasks";
import { TaskView } from "./pages/Projects/Tasks/TaskView";
import { ProjectNav } from "./pages/Projects/Views/ProjectNav";
import { ProjectIndex } from "./pages/Projects/Views/ProjectInfo";
import { ProjectList } from "./pages/Projects/Views/ProjectList";
// import { PrivateRoute } from "./base/PrivateRoute";
import TaskComments from "./pages/Projects/Tasks/TaskComments";
import { Route } from "react-router";
import AttachmentsDetailView from "./pages/Projects/Components/AttachmentsDetailView";
import { ThemeProvider } from "glamorous";
import { theme } from "./layouts/theme";
export const UIkit: any = require("uikit");
const Icons: any = require("uikit/dist/js/uikit-icons");

export interface IView {
	name: string;
	path: string;
	exact: boolean;
	component: any;
	children?: IView[];
	isSub?: boolean;
}

const Views: IView[] = [
	{
		name: "Home",
		path: "/",
		exact: true,
		component: Home
	},
	{
		name: "Info",
		path: "/projects",
		exact: false,
		component: ProjectNav,
		isSub: false
	},
	{
		name: "Projects",
		path: "/projects",
		exact: false,
		component: Projects,
		children: [
			{
				name: "Projects",
				path: "/projects",
				exact: true,
				component: ProjectList
			},
			{
				name: "Index",
				path: "/projects/:projectId",
				exact: true,
				component: ProjectIndex
			},
			{
				name: "Tasks",
				path: "/projects/:projectId/tasks",
				exact: false,
				component: ProjectTasks
			},
			{
				name: "Tasks Detail",
				path: "/projects/:projectId/tasks/:taskId/detail",
				exact: false,
				component: TaskView
			},
			{
				name: "Tasks Comments",
				path: "/projects/:projectId/tasks/:taskId/comments",
				exact: true,
				component: TaskComments
			},
			{
				name: "Tasks Attachments",
				path: "/projects/:projectId/tasks/:taskId/attachments",
				exact: true,
				component: AttachmentsDetailView
			}
		]
	},
	{
		name: "Calendar",
		path: "/calendar",
		exact: true,
		component: Calendar
	}
];

function _createRoutes(pages: IView[]): {}[] {
	let routes: any = [];

	pages.forEach((page: IView) => {
		if (page.component) {
			if (!page.children) {
				routes.push(
					<Route exact={page.exact} path={page.path} component={page.component} key={shortid.generate()} />
				);
			} else {
				routes.push(
					<Route
						exact={page.exact}
						path={page.path}
						component={() => {
							return (
								<page.component
									nodes={page!.children!.map(obj => ({
										name: obj.name,
										path: obj.path
									}))}
								>
									{_getRoutes(page.children!)}
								</page.component>
							);
						}}
						key={shortid.generate()}
					/>
				);
			}
		}
	});
	return routes;
}

function _getRoutes(views: IView[]) {
	let routes: any = [];

	routes = _createRoutes(views);
	return routes;
}

const App = () => {
	return (
		<ThemeProvider theme={theme}>
			<div className="app">
				<div className="app-header uk-navbar uk-light">
					<RightMenu views={Views} />
				</div>
				<div className="app-content">{_getRoutes(Views)}</div>

				<div className="app-footer">Footer </div>
			</div>
		</ThemeProvider>
	);
};

export default App;

UIkit.use(Icons);
