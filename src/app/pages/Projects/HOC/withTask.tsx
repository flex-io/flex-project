import React, { FC } from "react";
import {
	ProjectTask,
	useViewProjectTaskQuery,
	useUpdateProjectTaskFieldMutation,
	useDeleteProjectTaskMutation,
	ListProjectTaskByProjectDocument,
	TaskTodo,
	useListTaskTodoByTaskQuery,
	useUpdateTaskTodoStringFieldMutation,
	useUpdateTaskTodoBooleanFieldMutation,
	ListTaskTodoByTaskDocument,
	useCreateTaskTodoMutation,
	useDeleteTaskTodoMutation,
	useStatusTypesQuery,
	User,
	useListUsersQuery,
	ViewProjectTaskDocument,
	useCreateAttachmentMutation,
	useGetAttachmentsBySysPathQuery,
	Attachment,
	GetAttachmentsBySysPathDocument,
} from "codegen/graphql";
import { RouteComponentProps, withRouter } from "react-router";

export interface TaskComponentProps {
	projectTask?: ProjectTask;
	updateField?(field: string, value: any): void;
	deleteTask?(taskId: string, projectType: string): void;
	taskTodos?: TaskTodo[];
	updateTodoStringField?(id: string, field: string, value: string): void;
	updateTodoBooleanField?(id: string, field: string, value: boolean): void;
	createTaskTodo?(todo: string): void;
	deleteTaskTodo?(todoId: string): void;
	statusEnum?: { id: string; text: string }[];
	listUser?: User[];
	createAttachment?(sysPath: string, filename: string, filetype: string): void;
	attachments?: Attachment[];
}

export default function withTask<T extends RouteComponentProps<{ taskId: string; projectId: string }>>(
	Component: React.ComponentType<TaskComponentProps>
) {
	type InternalProps = T;

	const Task: FC<InternalProps> = props => {
		const taskId = props.match.params.taskId;
		const projectType = props.match.params.projectId;

		const { data, loading, error } = useViewProjectTaskQuery({
			variables: { projectType: projectType, taskId: taskId }
		});

		const [createAttachment] = useCreateAttachmentMutation();
		const attachments = useGetAttachmentsBySysPathQuery({
			variables: {
				sysPath: `task/${taskId}/`
			}
		});

		const [updateField] = useUpdateProjectTaskFieldMutation();
		const [deleteTask] = useDeleteProjectTaskMutation();

		const todos = useListTaskTodoByTaskQuery({
			variables: { taskType: taskId }
		});
		const [updateTodoStringField] = useUpdateTaskTodoStringFieldMutation();
		const [updateTodoBooleanField] = useUpdateTaskTodoBooleanFieldMutation();
		const [createTaskTodo] = useCreateTaskTodoMutation();
		const [deleteTodo] = useDeleteTaskTodoMutation();

		const statusTypes = useStatusTypesQuery();
		const listUser = useListUsersQuery();

	
		function CreateAttachment(sysPath: string, filename: string, filetype: string) {
	
			createAttachment({
				variables: {
					sysPath,
					filename,
					filetype
				},
				refetchQueries: [
					{
						query: GetAttachmentsBySysPathDocument,
						variables: { sysPath: `task/${taskId}/` }
					}
				]
			});
		}

		function UpdateField(field: string, value: any) {
		
			updateField({
				variables: {
					projectType,
					taskId,
					field,
					value
				},

				refetchQueries: [
					{
						query: ListProjectTaskByProjectDocument,
						variables: { projectType }
					},
					{
						query: ViewProjectTaskDocument,
						variables: { projectType, taskId: taskId }
					}
				]
			});
		}
		function DeleteTask() {
			deleteTask({
				variables: { id: taskId, projectType },
				refetchQueries: [
					{
						query: ListProjectTaskByProjectDocument,
						variables: { projectType }
					}
				]
			}).then(() => props.history.replace(`/projects/${projectType}/tasks`));
		}

		function DeleteTodo(todoId: string) {
			deleteTodo({
				variables: {
					id: todoId,
					taskType: taskId
				},
				refetchQueries: [
					{
						query: ListTaskTodoByTaskDocument,
						variables: { taskType: taskId }
					}
				]
			});
		}

		function UpdateTodoStringField(id: string, field: string, value: string) {
			updateTodoStringField({
				variables: {
					id,
					taskId,
					field,
					value
				}
			});
		}

		function UpdateTodoBooleanField(id: string, field: string, value: boolean) {
			updateTodoBooleanField({
				variables: {
					id,
					taskId,
					field,
					value
				},
				refetchQueries: [
					{
						query: ListTaskTodoByTaskDocument,
						variables: { taskType: taskId }
					}
				]
			});
		}

		function CreateTaskTodo(todo: string) {
			console.log("Triggering Create Task Todo")
			createTaskTodo({
				variables: {
					taskType: taskId,
					todo
				},
				refetchQueries: [
					{
						query: ListTaskTodoByTaskDocument,
						variables: { taskType: taskId }
					}
				]
			});
		}

		if (loading || todos.loading || statusTypes.loading || listUser.loading) return <div>Loading..</div>;
		if (!error === undefined) return <div>Error</div>;
		if (data.viewProjectTask === null) return <div>No data to display</div>;

		return (
			<Component
				projectTask={data.viewProjectTask}
				{...props}
				updateField={UpdateField}
				deleteTask={DeleteTask}
				taskTodos={todos.data.listTaskTodoByTask}
				updateTodoBooleanField={UpdateTodoBooleanField}
				updateTodoStringField={UpdateTodoStringField}
				createTaskTodo={CreateTaskTodo}
				deleteTaskTodo={DeleteTodo}
				statusEnum={statusTypes.data.statusTypes}
				listUser={listUser.data.listUsers}
				createAttachment={CreateAttachment}
				attachments={attachments.data.getAllAttachmentBySysPath}
			/>
		);
	};

	return withRouter(Task);
}
