import React, { FC } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import {
	TaskComment,
	useGetTaskCommentsByTaskQuery,
	usePostCommentMutation,
	GetTaskCommentsByTaskDocument
} from "codegen/graphql";

export interface TaskCommentProps {
	postComment(comment: string): void;
	commentList: TaskComment[];
}

export default function withTaskComment<T extends RouteComponentProps<{ taskId: string; projectId: string }>>(
	Component: React.ComponentType<TaskCommentProps>
) {
	type InternalProps = T;

	const Task: FC<InternalProps> = props => {
		const taskType = props.match.params.taskId;

		const { data, loading, error } = useGetTaskCommentsByTaskQuery({
			variables: { taskType }
		});
		const [postComment] = usePostCommentMutation({});

		function PostComment(comment: string) {
			postComment({
				variables: {
					postMessage: comment,
					taskType
				},
				refetchQueries: [
					{
						query: GetTaskCommentsByTaskDocument,
						variables: { taskType }
					}
				]
			});
		}

		if (loading) return <div>Loading..</div>;
		if (!error === undefined) return <div>Error</div>;
		if (data.getTaskCommentsByTask === null || data.getTaskCommentsByTask === undefined)
			return <div>No data to display</div>;

		return <Component {...props} postComment={PostComment} commentList={data.getTaskCommentsByTask} />;
	};

	return withRouter(Task);
}
