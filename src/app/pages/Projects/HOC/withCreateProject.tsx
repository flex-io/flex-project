import React, { FC } from "react";
// import { CreateProjectProps } from "codegen/graphql";
import { RouteComponentProps } from "react-router";
import { Subtract } from "utility-types";

type Props = RouteComponentProps;

function withCreateProject<T extends Props>(Component: React.ComponentType<T>) {
	return class extends React.Component<Subtract<T, Props>> {
		render() {
			return <Component {...this.props as T} />;
		}
	};
}

export default withCreateProject;


const projectCreateView: FC<Props> = (props) => {
    return <div {...props}>Test</div>
}

export const ProjectCreateView = withCreateProject(projectCreateView)
