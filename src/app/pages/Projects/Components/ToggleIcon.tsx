import React, { FC } from "react";

interface ToggleIconProps {
	onIcon?: string;
	offIcon?: string;
	toggled?: boolean;
	onToggle?(): void;
}

export const ToggleIcon: FC<ToggleIconProps> = props => {
	return (
		<a
			href=" "
			uk-icon={`icon: ${props.toggled ? props.offIcon : props.onIcon}`}
			onClick={e => {
				e.preventDefault();
				props.onToggle();
			}}
		/>
	);
};
ToggleIcon.defaultProps = {
	onIcon: "triangle-down",
	offIcon: "triangle-up",
	toggled: false
};
