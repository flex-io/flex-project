import React, { FC } from "react";
import DetailView, { DetailFooter } from "components/Flex/DetailView";
import {
	SectionItemInput,
	SectionItemDropdownMenu,
	SectionItemCheckbox,
	SectionItemLinkAnchor,
	SectionItemDatePicker2,
} from "components/Flex/DetailView/SectionItem";
import IconButton from "components/uikit/Button/IconButton";

const AttachmentsDetailView: FC<{}> = () => {
	return (
		<DetailView>
			<DetailView.Header>
				<DetailView.Section noHover={true}>
					<SectionItemInput defaultValue="Hello there" Icon="comments" />
				</DetailView.Section>
			</DetailView.Header>

			<DetailView.Body>
				<DetailView.Section header="Fields" expand={true} collapsible={true}>
					<SectionItemDropdownMenu
						subText="Test"
						placeholder="Select"
						list={[{ id: "1234", text: "1234" }]}
					/>
					<SectionItemInput defaultValue="Ronald Manzano" onChanged={value => console.log(value)} />
					<SectionItemCheckbox
						onTextChanged={val => console.log(val)}
						onCheckChanged={val => console.log(val)}
					/>
					<SectionItemLinkAnchor
						link="https://flex-notes-app-uploads.s3-ap-southeast-2.amazonaws.com/public/task/Y3KkCvkm0/956a%20solomon.pdf"
						text="956a solomon.pdf"
						subText="42 kbs"
						onRemove={() => console.log("removing")}
					/>
					<SectionItemDatePicker2
						defaultValue={new Date()}
						placeholder="Set Due"
						label="Due On"
						onChanged={value => console.log(value)}
						Icon="calendar"
					/>
				</DetailView.Section>
			</DetailView.Body>
			<DetailFooter>
				<DetailFooter.LeftPane>
					<IconButton icon="comments" />
				</DetailFooter.LeftPane>
				<DetailFooter.CenterPane>
					<div style={{ fontSize: 12 }}>Created on Fri, August 11</div>
				</DetailFooter.CenterPane>
				<DetailFooter.RightPane>
					<IconButton icon="trash" />
				</DetailFooter.RightPane>
			</DetailFooter>
		</DetailView>
	);
};

export default AttachmentsDetailView;
