import React from "react";
import SplitContainer from "../../../../components/Containers/SplitContainer";
import { Link } from 'react-router-dom';

//TODO Project Header
//TODO Header > Actions > New Project
//TODO When detail click > proceed to /projects/:id
//TODO Table View

export default class index extends React.Component<{}> {
	render() {
		return (
			<SplitContainer>
				{({ S1, S2 }) => (
					<>
						<S1>
							<div>Projects</div>
							<Link to="/projects/new">New</Link>
						</S1>
						<S2>{this.props.children}</S2>
					</>
				)}
			</SplitContainer>
		);
	}
}
