import React, { FC } from "react";
import glamorous from "glamorous";

const BaseAdd = glamorous.div({
	// boxShadow: "0 -17px 0 -16px #e5e5e5",
	// margin: "0 8px",
	padding: 16,
	fontSize: "1rem",
	display: "flex",
	// minHeight: 52,
	"> *": {
		alignSelf: "center"
	}
});

const Input = glamorous.input<React.InputHTMLAttributes<HTMLInputElement>>({
	fontSize: "14px",
	border: "none !important",
	outline: "none",
	paddingLeft: "14px",
	width: "100%",
	"::placeholder": {
		color: "#0F6DCD"
	},
	":focus": {
		"::placeholder": {
			color: "#9A9A9A"
		}
	}
});

interface AddTodoProps {
	onChanged(value: string): void;
}

export const AddTodo: FC<AddTodoProps> = props => {
	let TodoInput: any;
	return (
		<BaseAdd>
			<span uk-icon="icon: plus" />
			<label htmlFor="baseAddInput-addTodo" />
			<Input
				innerRef={ref => (TodoInput = ref)}
				id="baseAddInput-addTodo"
				type="text"
				placeholder="Add Todo"
				onKeyDown={e => {
					if (e.key === "Enter" && TodoInput.value.trim() !== "") {
						props.onChanged(TodoInput.value);
						TodoInput.value = "";
					} else if (e.key === "Escape") {
						TodoInput.blur();
						TodoInput.value = "";
					}
				}}
			/>
		</BaseAdd>
	);
};
