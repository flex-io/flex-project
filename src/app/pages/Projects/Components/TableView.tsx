import React from "react";
// import { Link } from "react-router-dom";
import Table from "../../../../components/Table";
import { Link } from "react-router-dom";

const ProjectHeaders = [
	{
		key: "id",
		title: "id",
		dataIndex: "id",
		visible: false
	},
	{
		key: "projectName",
		title: "Project",
		dataIndex: "projectName",
		render: (row: { id: string; projectName: string }) => (
			<Link to={`/projects/${row.id}`}>{row.projectName}</Link>
		)
	},
	{
		key: "description",
		title: "description",
		dataIndex: "description"
	},
	{
		key: "dateCompletion",
		title: "dateCompletion",
		dataIndex: "dateCompletion"
	},
	{
		key: "ownerUserId",
		title: "ownerUserId",
		dataIndex: "ownerUserId"
	}
];

export default (props: { data: any[] }) => {
	return <Table headings={ProjectHeaders} rows={props.data} />;
};
