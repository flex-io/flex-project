import React, { FC, useRef } from "react";
import glamorous from "glamorous";

const BaseAdd = glamorous.div({
	boxShadow: "0 -17px 0 -16px #e5e5e5",
	margin: "0 8px",
	padding: "0 12px",
	fontSize: "1rem",
	display: "flex",
	minHeight: 52,
	"> *": {
		alignSelf: "center"
	}
});

const Input = glamorous.input<React.InputHTMLAttributes<HTMLInputElement>>({
	fontSize: "1rem",
	border: "none !important",
	outline: "none",
	width: "100%",
	"::placeholder": {
		color: "#0F6DCD"
	},
	":focus": {
		"::placeholder": {
			color: "#9A9A9A"
		}
	}
});

interface AddTaskProps {
	onChanged(value: string): void;
}

export const AddTask: FC<AddTaskProps> = props => {
	const inputEl = useRef(null);
	return (
		<BaseAdd>
			<span uk-icon="icon: plus" />
			<label htmlFor="baseAddInput-addTask" />
			<Input
				innerRef={inputEl}
				id="baseAddInput-addTask"
				type="text"
				placeholder="Add task"
				onKeyDown={e => {
					if (e.key === "Enter") {
						props.onChanged(inputEl.current.value);
						inputEl.current.blur();
						inputEl.current.value = "";
					} else if (e.key === "Escape") {
						inputEl.current.blur();
						inputEl.current.value = "";
					}
				}}
			/>
		</BaseAdd>
	);
};
