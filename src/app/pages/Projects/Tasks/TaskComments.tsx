import React, { FC } from "react";
import CommentList from "components/Views/Detail/Components/CommentList";
import withTaskComment, { TaskCommentProps } from "../HOC/withTaskComment";

const taskComment: FC<TaskCommentProps> = props => {
	let comments = [];
	if (props.commentList) {
		comments = props.commentList.map(user => {
			return {
				userId: user.userType,
				text: user.postMessage
			};
		});

		return <CommentList comments={comments} callbackMessage={val => props.postComment(val)} />;
	}else return <div>Nothing</div>
};

const TaskComment = withTaskComment(taskComment);

export default TaskComment;
