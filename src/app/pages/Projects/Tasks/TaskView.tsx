import React, { FC } from "react";
// import { Detail } from "components/Views/Detail/Detail";
import withTask, { TaskComponentProps } from "../HOC/withTask";
// import { AddTodo } from "../Components/AddTodo";
// import { shortid } from "helper";
// import { ToggleText } from "components/Toggle";
// import glamorous from "glamorous";
// import Upload from "components/uikit/Upload";

// import { Storage } from "aws-amplify";
// import config from "../../../../config";
import _ from "lodash";

// import S3BucketList from "components/Views/Detail/Components/S3BucketList";
import DetailView, { DetailFooter } from "components/Flex/DetailView";
import {
	SectionItemInput,
	SectionItemDropdownMenu,
	SectionItemCheckbox,
	SectionItemLinkAnchor,
	SectionItemDatePicker2,
	SectionItemAddInput
} from "components/Flex/DetailView/SectionItem";
import IconButton from "components/uikit/Button/IconButton";
import { shortid } from "helper";

// const FlexLeftAndRight = glamorous.div({
// 	display: "flex"
// });

// const HeaderStyle = glamorous.div({
// 	flex: 1
// });

// const UploadFile = glamorous.input({});

const projectTaskView: FC<TaskComponentProps> = props => {
	// const upload = useRef(null);
	const projectType = props.projectTask.projectType.id;
	const taskId = props.projectTask.id;
	const { deleteTask, updateField, projectTask } = props;
	// const [isShowCompleted, showCompleted] = useState(true);

	// const uploadSysPath = `task/${taskId}`;

	const listUser = props.listUser.map(user => {
		return {
			id: user.id,
			text: user.firstName
		};
	});

	return (
		<DetailView>
			<DetailView.Header>
				<DetailView.Section noHover={true}>
					<SectionItemInput
						defaultValue={projectTask.taskName}
						onChanged={value => updateField("taskName", value)}
						Icon="comments"
					/>
				</DetailView.Section>
			</DetailView.Header>

			<DetailView.Body>
				<DetailView.Section header="Fields" expand={true} collapsible={true}>
					<SectionItemDropdownMenu
						subText="OWNER"
						placeholder="Select owner"
						list={listUser}
						defaultValue={projectTask.ownerUserId}
						onChanged={id => {
							updateField("ownerUserId", id);
						}}
					/>
					<SectionItemDropdownMenu
						subText="PRIORITY"
						defaultValue={projectTask.priority}
						placeholder="Set Priority"
						list={[
							{ id: "Low", text: "Low" },
							{ id: "Medium", text: "Medium" },
							{ id: "High", text: "High" }
						]}
						onChanged={id => {
							// console.log(val);
							updateField("priority", id);
						}}
					/>
					<SectionItemDropdownMenu
						subText="TASK STATUS"
						defaultValue={projectTask.statusType}
						list={props.statusEnum}
						placeholder="Select Status"
						onChanged={val => {
							// console.log(val);
							updateField("statusType", val);
						}}
					/>
					<SectionItemInput defaultValue="Ronald Manzano" onChanged={value => console.log(value)} />

					<SectionItemLinkAnchor
						link="https://flex-notes-app-uploads.s3-ap-southeast-2.amazonaws.com/public/task/Y3KkCvkm0/956a%20solomon.pdf"
						text="956a solomon.pdf"
						subText="42 kbs"
						onRemove={() => console.log("removing")}
					/>
					<SectionItemDatePicker2
						defaultValue={projectTask.dateStart}
						placeholder="Set Start"
						label="Date Start"
						onChanged={value => updateField("dateStart", value)}
						Icon="calendar"
					/>
				</DetailView.Section>

				<DetailView.Section header="Todos" expand={true} collapsible={true}>
					{props.taskTodos &&
						props.taskTodos.map(todo => {
							return (
								<React.Fragment key={shortid.generate()}>
									<SectionItemCheckbox
										onRemove={() => props.deleteTaskTodo(todo.id)}
										isCheck={todo.isCompleted}
										text={todo.todo}
										onCheckChanged={val =>
											props.updateTodoBooleanField(todo.id, "isCompleted", val)
										}
										// onDelete={() => props.deleteTaskTodo(todo.id)}
									/>
								</React.Fragment>
							);
						})}
					<SectionItemAddInput
						placeholder="Add Todo"
						onChanged={value => {
							// console.log("testing");
							props.createTaskTodo(value);
						}}

						// onTextChanged={val => console.log(val)}
						// onCheckChanged={val => console.log(val)}
					/>
				</DetailView.Section>
			</DetailView.Body>
			<DetailFooter>
				<DetailFooter.LeftPane>
					<IconButton icon="comments" />
				</DetailFooter.LeftPane>
				<DetailFooter.CenterPane>
					<div style={{ fontSize: 12 }}>Created on Fri, August 11</div>
				</DetailFooter.CenterPane>
				<DetailFooter.RightPane>
					<IconButton icon="trash" onClick={() => deleteTask(taskId, projectType)} />
				</DetailFooter.RightPane>
			</DetailFooter>
		</DetailView>
		// <>
		// 	<Detail
		// 		actions={[
		// 			{
		// 				key: "delete",
		// 				text: "Delete",
		// 				callback: () => deleteTask(taskId, projectType)
		// 			}
		// 		]}
		// 		title={projectTask.taskName}
		// 		onTitleUpdate={value => updateField("taskName", value)}
		// 	>
		// 		<Detail.Section>
		// 			{({ Select }) => (
		// 				<>
		// 					<Select
		// 						defaultValue={projectTask.ownerUserId}
		// 						options={listUser}
		// 						onChanged={value => {
		// 							console.log(value);
		// 							updateField("ownerUserId", value);
		// 						}}
		// 						defaultText="Assign to"
		// 					/>
		// 					<Select
		// 						defaultValue={projectTask.priority}
		// 						options={[
		// 							{ id: "Low", text: "Low" },
		// 							{ id: "Medium", text: "Medium" },
		// 							{ id: "High", text: "High" }
		// 						]}
		// 						onChanged={value => updateField("priority", value)}
		// 						defaultText="Set Priority"
		// 					/>
		// 					<Select
		// 						defaultValue={projectTask.statusType}
		// 						options={props.statusEnum}
		// 						onChanged={value => updateField("statusType", value)}
		// 						defaultText="Select Status"
		// 					/>
		// 				</>
		// 			)}
		// 		</Detail.Section>
		// 		<Detail.Section>
		// 			{({ Datepicker }) => (
		// 				<>
		// 					<Datepicker
		// 						defaultValue={projectTask.dateStart}
		// 						caption="Start Date: "
		// 						icon="clock"
		// 						onChanged={value => updateField("dateStart", value)}
		// 					/>
		// 					<Datepicker
		// 						defaultValue={projectTask.dateEnd}
		// 						caption="End Date: "
		// 						icon="clock"
		// 						onChanged={value => updateField("dateEnd", value)}
		// 					/>
		// 				</>
		// 			)}
		// 		</Detail.Section>
		// 		<Detail.Section
		// 			Header={() => {
		// 				return (
		// 					<FlexLeftAndRight>
		// 						<HeaderStyle>Attachment</HeaderStyle>
		// 						{/* <Button
		// 							onClick={e => {
		// 								e.preventDefault();
		// 								Storage.configure({
		// 									bucket: config.s3.BUCKET,
		// 									region: config.s3.REGION,
		// 									identityPoolId: config.cognito.IDENTITY_POOL_ID
		// 								});

		// 								// Storage.get("task/Y3KkCvkm0/Screen+Shot+2019-07-30+at+3.20.40+pm.png", {
		// 								// 	level: "public"
		// 								// }).then(res => console.log(res));
		// 								Storage.list("task/Y3KkCvkm0", {level: "public"}).then(res => console.log(res));
		// 							}}
		// 						>
		// 							Test
		// 						</Button> */}
		// 						<Upload
		// 							selectedFiles={files => {
		// 								Storage.configure({
		// 									bucket: config.s3.BUCKET,
		// 									// level: "private",
		// 									region: config.s3.REGION,
		// 									identityPoolId: config.cognito.IDENTITY_POOL_ID
		// 								});
		// 								// console.log([files])
		// 								_.forEach(files, file => {
		// 									console.log(file)
		// 									Storage.put(`${uploadSysPath}/${file.name}`, file, {
		// 										// contentType: file.type === "" ? null : file.type,
		// 										level: "public"
		// 									})
		// 								});
		// 							}}
		// 						/>
		// 					</FlexLeftAndRight>
		// 				);
		// 			}}
		// 		>
		// 			{({}) => (
		// 				<>
		// 					<S3BucketList path={uploadSysPath} level="public" />
		// 				</>
		// 			)}
		// 		</Detail.Section>
		// 		<Detail.Section
		// 			Header={() => {
		// 				return (
		// 					<FlexLeftAndRight>
		// 						<HeaderStyle>Todo</HeaderStyle>
		// 						<ToggleText
		// 							toggleOffText="Show Completed"
		// 							toggleOnText="Hide Completed"
		// 							isToggle={isShowCompleted}
		// 							onChanged={val => {
		// 								showCompleted(val);
		// 							}}
		// 						/>
		// 					</FlexLeftAndRight>
		// 				);
		// 			}}
		// 		>
		// 			{({ Checkbox }) => (
		// 				<>
		// 					{props.taskTodos &&
		// 						props.taskTodos
		// 							.filter(todo => {
		// 								return todo.isCompleted === false || isShowCompleted === true;
		// 							})
		// 							.map(todo => {
		// 								return (
		// 									<React.Fragment key={shortid.generate()}>
		// 										<Checkbox
		// 											defaultChecked={todo.isCompleted}
		// 											text={todo.todo}
		// 											onChanged={val =>
		// 												props.updateTodoBooleanField(todo.id, "isCompleted", val)
		// 											}
		// 											onDelete={() => props.deleteTaskTodo(todo.id)}
		// 											strikeThrough={todo.isCompleted}
		// 										/>
		// 									</React.Fragment>
		// 								);
		// 							})}

		// 					<AddTodo
		// 						onChanged={val => {
		// 							props.createTaskTodo && props.createTaskTodo(val);
		// 						}}
		// 					/>
		// 				</>
		// 			)}
		// 		</Detail.Section>
		// 	</Detail>
		// </>
	);
};

export const TaskView = withTask(projectTaskView);

// <input
// 								type="file"
// 								innerRef={upload}
// 								// accept="image/png, image/jpeg"
// 								onChange={() => {
// 									// console.log(upload.current.files[0])
// 									Storage.configure({
// 										bucket: config.s3.BUCKET,
// 										// level: "private",
// 										region: config.s3.REGION,
// 										identityPoolId: config.cognito.IDENTITY_POOL_ID
// 									});
// 									Storage.put(
// 										uploadSysPath.concat(upload.current.files[0].name),
// 										upload.current.files[0],
// 										{
// 											contentType: upload.current.files[0].type,
// 											level: "public"
// 										}
// 									).then(() => {
// 										props.createAttachment(
// 											uploadSysPath,
// 											upload.current.files[0].name,
// 											upload.current.files[0].type
// 										);
// 									});
// 								}}
// 							/>
