import React, { FC } from "react";
import { Button } from "components/uikit";
import { RouteComponentProps, withRouter } from "react-router";
import { useDeleteProjectMutation, ListProjectsDocument } from "codegen/graphql";

const projectIndex: FC<RouteComponentProps<{ projectId: string }>> = props => {
	const [deleteProject] = useDeleteProjectMutation();
	return (
		<div>
			<Button
				button_type="link"
				onClick={e => {
					e.preventDefault();
					deleteProject({
						variables: { id: props.match.params.projectId },
						refetchQueries: [
							{
								query: ListProjectsDocument
							}
						]
					}).then(() => {
						props.history.push('/projects')
					});
				}}
			>
				Delete
			</Button>
		</div>
	);
};

export const ProjectIndex = withRouter(projectIndex);
