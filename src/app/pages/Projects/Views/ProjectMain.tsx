import React from "react";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";


interface Props extends RouteComponentProps<{ projectId: string }> {
	nodes?: { name: string; path: string }[];
}

class ProjectMain extends React.Component<Props> {
	render() {
		const { nodes } = this.props;
		return (
			<>
				<h1>Projects</h1>
				
				{nodes &&
					nodes.map(node => {
						return (
							<Link
								key={node.path}
								to={`${node.path.replace(":projectId", this.props.match.params.projectId)}`}
							>
								{node.name}
							</Link>
						);
					})}
				{this.props.children}
			</>
		);
	}
}

export default withRouter(ProjectMain);

// return React.createElement(
// 	"div",
// 	{},
// 	<>
// 		<h1>Header</h1>
// 		{React.Children.map(
// 			this.props.children,
// 			(child: JSX.Element) => (
// 				// Replace :id based on the params.id
// 				<>
// 					<Link
// 						to={`${String(child.key!).replace(
// 							":projectId",
// 							this.props.match.params.projectId
// 						)}`}
// 					>
// 						{child.props.component.name}
// 					</Link>
// 					{"  "}
// 				</>
// 			)
// 		)}
// 		{this.props.children}
// 	</>
// );
