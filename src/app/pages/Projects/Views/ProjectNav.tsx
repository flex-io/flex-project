import React, { FC } from "react";
import Tabs from "components/Tabs";

// import { compose } from "react-apollo";
import { Link as ReactRouterLink, LinkProps, RouteComponentProps } from "react-router-dom";
import glamorous, { GlamorousComponent } from "glamorous";
import { useListProjectsQuery } from "codegen/graphql";
// import glamorous from "glamorous";

const GLink: GlamorousComponent<LinkProps & {}, ReactRouterLink> = glamorous(ReactRouterLink)({
	textDecoration: "none",
	padding: "10px 0 0 10px"
	// color: "red"
});

const TabContainerMenu = glamorous.div({
	display: "flex",
	flexDirection: "column",
	minWidth: 200
});

const projectNav: FC<RouteComponentProps<{}>> = props => {
	const { data, loading, error } = useListProjectsQuery();
	if (loading) return <div>Loading..</div>;
	if (!error === undefined) return <div>Error..</div>;

	return (
		<Tabs
			tabIdList={
				data.listProjects &&
				data.listProjects.map(p => {
					return p.id;
				})
			}
			position="left"
			onClick={tab => {
				props.history.push(`/projects/${tab.id}`);
			}}
		>
			{data.listProjects &&
				data.listProjects.map(project => {
					return (
						<Tabs.Tab
							key={project.id}
							id={project.id}
							title={project.projectName.substr(0, 2).toUpperCase()}
						>
							<TabContainerMenu>
								{project.projectName}
								<GLink to={`/projects/${project.id}/tasks`}>Tasks</GLink>
								<GLink to={`/projects/${project.id}/settings`}>Settings</GLink>
							</TabContainerMenu>
						</Tabs.Tab>
					);
				})}
		</Tabs>
	);
};

export const ProjectNav = projectNav;

// const Flex = glamorous.div<{ direction?: any }>(
// 	{
//         display: "flex",
//         width: 250
// 	},
// 	props => {
// 		return {
// 			flexDirection: props.direction
// 		};
// 	}
// );

// export class ProjectNav extends PureComponent<{}> {
// 	render() {
// 		return (
// 			<Tabs position="left">
// 				<Tabs.Tab id="tab1" title="PR">
// 					<Flex direction="column">
// 						{"About"}
// 						<Tabs>

// 							<Tabs.Tab id="task" title="task">
// 								<ul>
// 									<li>Important</li>
// 									<li>Assign to Me</li>
// 								</ul>
// 							</Tabs.Tab>
//                             <Tabs.Tab id="member" title="members">
// 								<ul>
// 									<li>Information</li>
// 									<li>Anything</li>
// 								</ul>
// 							</Tabs.Tab>
// 						</Tabs>
// 					</Flex>
// 				</Tabs.Tab>
// 				<Tabs.Tab id="tab2" title="AV">
// 					<NavContainer title="Test 2" />
// 				</Tabs.Tab>
// 			</Tabs>
// 		);
// 	}
// }

// const Container = glamorous.div({
// 	display: "flex",
// 	flexDirection: "column"
// 	// borderBottom: "1px solid #dfdfdf"
// });

// interface NavContainerProps {
// 	title: string;
// }

// const NavContainer: FC<NavContainerProps> = props => {
// 	const { title } = props;
// 	return <Container>{title}</Container>;
// };
