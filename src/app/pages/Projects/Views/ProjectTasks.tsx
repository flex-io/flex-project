import React, { FC } from "react";
import { withRouter, RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";
import { shortid } from "helper";
import { AddTask } from "../Components/AddTask";
import {
	useCreateProjectTaskMutation,
	useListProjectTaskByProjectQuery,
	ListProjectTaskByProjectDocument
} from "codegen/graphql";
import glamorous from "glamorous";

type Props = RouteComponentProps<{ projectId: string }>;

const projectTasks: FC<Props> = props => {
	const projectTypeId: string = props.match.params.projectId;
	const taskLink: string = `/projects/${projectTypeId}/tasks/`;
	const [createTask] = useCreateProjectTaskMutation();
	const { data, loading } = useListProjectTaskByProjectQuery({
		variables: { projectType: props.match.params.projectId }
	});

	if (loading) return <div>Loading...</div>;

	return (
		<div className="app-content-stretch">
			<h4>Tasks</h4>
			<div className="scroll-container">
				<ComponentList>
					{data.listProjectTaskByProject.map(task => {
						return (
							<React.Fragment key={shortid.generate()}>
								<ListItem>
									<Left>
										<Link to={`${taskLink}${task.id}/detail`}>{task.taskName}</Link>
									
									</Left>
								</ListItem>
							</React.Fragment>
						);
					})}
				</ComponentList>
			</div>
			<AddTask
				onChanged={val =>
					createTask({
						variables: { taskName: val, projectType: projectTypeId, ownerUserId: "12345" },
						refetchQueries: [
							{
								query: ListProjectTaskByProjectDocument,
								variables: { projectType: projectTypeId }
							}
						]
					})
				}
			/>
			<div className="background-lines" />
		</div>
	);
};

// const FlexRow = glamorous.div({
// 	display: "flex",
// 	"> a": {
// 		marginRight: 10
// 	}

// });

const Left = glamorous.div({
	flex: 1,
	display: "flex",
	flexDirection: "column"
});

const ComponentList = glamorous.div({
	margin: "0 24px",
	display: "flex",
	flexDirection: "column"
});

const ListItem = glamorous.dt({
	minHeight: 52,
	borderBottom: "1px solid #e5e5e5",
	display: "flex",
	"> div": {
		alignSelf: "center"
	}
});

export const ProjectTasks = withRouter(projectTasks);
