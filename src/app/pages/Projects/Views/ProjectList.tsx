import React, { FC } from "react";
import { Form } from "components/uikit";
import BaseForm from "components/Forms/BaseForm";
import { useCreateProjectMutation, ListProjectsDocument } from "codegen/graphql";
import { Modal } from "components/Modal";

interface IProject {
	projectName: string;
	description: string;
	ownerUserId: string;
}

class CreateProjectForm extends BaseForm<IProject> {}

export const ProjectList: FC<{}> = () => {
	const [createProject] = useCreateProjectMutation();
	return (
		<div>
			<CreateProjectForm
				initialValue={{
					projectName: "",
					description: "",
					ownerUserId: ""
				}}
				FormComponent={({ fields, onChange }) => {
					return (
						<Modal
							text="Create Project"
							onClickOk={() => {
								createProject({
									variables: {
										ownerUserId: fields.ownerUserId,
										projectName: fields.projectName,
										description: fields.description
									},
									refetchQueries: [
										{
											query: ListProjectsDocument
										}
									]
								});
							}}
						>
							<Form>
								<Form.FormInput
									label="Project Name"
									value={fields.projectName}
									onChange={e => onChange("projectName", e.target.value)}
								/>
								<Form.FormInput
									label="Description"
									value={fields.description}
									onChange={e => onChange("description", e.target.value)}
								/>
								<Form.FormInput
									label="Owner"
									value={fields.ownerUserId}
									onChange={e => onChange("ownerUserId", e.target.value)}
								/>
							</Form>
						</Modal>
					);
				}}
			/>
		</div>
	);
};
