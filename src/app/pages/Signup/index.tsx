import React from "react";
import { withAuthenticator, Authenticator } from "aws-amplify-react";
import { RouteComponentProps, withRouter } from "react-router";

const AlwaysOn = (props: any) => {
	return (
		<div>
			<div>I am always here to show current auth state: {props.authState}</div>
			<button onClick={() => props.onStateChange("signUp")}>Show Sign Up</button>
		</div>
	);
};

const signUpConfig = {
	header: "My Customized Sign Up",
	hideAllDefaults: true,
	defaultCountryCode: "1",
	authenticatorComponents: [AlwaysOn],
	signUpFields: [
		{
			label: "Email",
			key: "email",
			required: true,
			displayOrder: 1,
			type: "string"
		},
		{
			label: "Password",
			key: "password",
			required: true,
			displayOrder: 2,
			type: "password"
		},
		{
			label: "PhoneNumber",
			key: "phone_number",
			required: true,
			displayOrder: 3,
			type: "string"
		},
		{
			label: "Custom Attribute",
			key: "custom_attr",
			required: false,
			displayOrder: 4,
			type: "string",
			custom: true
		}
	]
};

interface State {
	authState: any;
}

class signup1 extends React.Component<RouteComponentProps, State> {
	handleAuthStateChange(state: any) {
		if (state === "signedIn") {
			/* Do something when the user has signed-in */
			this.props.history.push("/");
		}
	}
	render() {
		return <Authenticator usernameAttributes="email" onStateChange={this.handleAuthStateChange} />;
	}
}

const signup = withRouter(signup1);

const Signup = withAuthenticator(signup, { signUpConfig });

export default Signup;
