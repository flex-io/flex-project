import React, { FC, useEffect } from "react";
import BaseForm from "components/Forms/BaseForm";
import { Form, Button } from "components/uikit";
import { Auth } from "aws-amplify";
import { withRouter, RouteComponentProps } from "react-router";
import { getAccessToken } from "helper";



interface IAuth {
	username: string;
	password: string;
}

class LoginForm extends BaseForm<IAuth> {}

const login: FC<RouteComponentProps> = props => {
	async function AuthCredentials(username: string, password: string) {
		await Auth.signIn(username, password)
			.then(() => {
				props.history.push("/");
			})
			.catch(e => alert(e));
	}

	useEffect(() => {
		const logout = async () => {
			if (getAccessToken !== null) {
				await Auth.signOut();
			}
		};

		logout();
	}, []);

	return (
		<LoginForm
			initialValue={{
				username: "",
				password: ""
			}}
			FormComponent={({ fields, onChange }) => {
				return (
					<>
						<Form>
							<Form.FormInput
								label="Username"
								value={fields.username}
								type="text"
								onChange={e => onChange("username", e.target.value)}
							/>
							<Form.FormInput
								label="Password"
								value={fields.password}
								type="password"
								onChange={e => onChange("password", e.target.value)}
							/>
							<Form.FormControls>
								<Button
									button_type="primary"
									onClick={e => {
										e.preventDefault();
										AuthCredentials(fields.username, fields.password);
									}}
								>
									Login
								</Button>
							</Form.FormControls>
						</Form>
					</>
				);
			}}
		/>
	);
};
const Login = withRouter(login);
export default Login;
