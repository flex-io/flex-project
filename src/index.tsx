import React, { Component } from "react";
import ReactDOM from "react-dom";
import App from "./app";
import { Router, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import Amplify, { Auth, Storage } from "aws-amplify";
import { ApolloLink } from "apollo-link";
import config from "./config";
import url from "url";
import aws4 from "aws4";
// import Login from "app/pages/Login";
// import Signup from "app/pages/Signup";
import { Authenticator, Greetings } from "aws-amplify-react";

Amplify.configure({
	Auth: {
		// OPTIONAL - Enforce user authentication prior to accessing AWS resources or not
		mandatorySignIn: false,
		region: config.cognito.REGION,
		userPoolId: config.cognito.USER_POOL_ID,
		// REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
		identityPoolId: config.cognito.IDENTITY_POOL_ID,
		userPoolWebClientId: config.cognito.APP_CLIENT_ID
	},
	Storage: {
		region: config.s3.REGION,
		bucket: config.s3.BUCKET,
		identityPoolId: config.cognito.IDENTITY_POOL_ID
	},
	API: {
		endpoints: [
			{
				name: "graphql",
				endpoint: config.apiGateway.URL,
				region: config.apiGateway.REGION
			}
		]
	}
});

Storage.configure({
	bucket: config.s3.BUCKET,
	region: config.s3.REGION,
	identityPoolId: config.cognito.IDENTITY_POOL_ID
});

const history = createBrowserHistory();

async function awsGraphqlFetch(uri: any, options: any) {
	options = options || {};

	const urlObject = url.parse(uri);
	const signable: any = {
		host: urlObject.host,
		path: urlObject.path
	};
	["method", "body", "headers", "region", "service"].forEach(key => (signable[key] = options[key]));

	const aFetch = Auth.currentCredentials().then(data => {
		const credentials = {
			secretAccessKey: data.secretAccessKey,
			accessKeyId: data.accessKeyId,
			sessionToken: data.sessionToken
		};
		aws4.sign(signable, credentials);
		options.headers = signable.headers;
		// options.credentials = { projectId: "test" };
		options.myData = { projectid: "test" };

		return fetch(uri, options);
	});

	return await aFetch;
}

export const client = new ApolloClient({
	link: ApolloLink.from([
		new HttpLink({
			// uri: "https://4kne7cve60.execute-api.ap-southeast-2.amazonaws.com/dev/graphql",
			uri: "http://localhost:3001/graphql",
			fetch: awsGraphqlFetch
		})
	]),
	cache: new InMemoryCache()
});

class AppWithAuth extends Component {
	state = {
		authState: ""
	};

	onAuthStateChanged(authState: string) {
		// signedUp
		// signedIn
		// signUp
		// confirmSignUp

		this.setState({ authState });
		if (authState === "signedUp") {
			//TODO insert to userTable
		}
	}
	render() {
		console.log(this.state.authState);
		return (
			<>
				<Authenticator
					hide={[Greetings]}
					// hideDefault={true}
					usernameAttributes="email"
					onStateChange={this.onAuthStateChanged.bind(this)}
				>
					{this.state.authState === "signedIn" && <App />}
				</Authenticator>
			</>
		);
	}
}

ReactDOM.render(
	<Router history={history}>
		<Switch>
			<ApolloProvider client={client}>
				<ApolloHooksProvider client={client}>
					<AppWithAuth />
				</ApolloHooksProvider>
			</ApolloProvider>
		</Switch>
	</Router>,
	document.getElementById("root")
);
