export default {
	s3: {
		REGION: "ap-southeast-2",
		BUCKET: "flex-notes-app-uploads"
	},
	apiGateway: {
		REGION: "ap-southeast-2",
		URL: "https://4kne7cve60.execute-api.ap-southeast-2.amazonaws.com/dev"
	},
	cognito: {
		REGION: "ap-southeast-2",
		USER_POOL_ID: "ap-southeast-2_zR6aIVSDz",
		APP_CLIENT_ID: "34q4uuh2k4vv8tbjk5l31qc9kl",
		IDENTITY_POOL_ID: "ap-southeast-2:4a70a2a2-5d2d-492e-bdf5-ac60f1d0e973"
	},
	MAX_ATTACHMENT_SIZE: 5000000
};
