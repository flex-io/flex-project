import gql from "graphql-tag";
import * as ReactApolloHooks from "react-apollo-hooks";
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Attachment = {
  __typename?: "Attachment";
  id: Scalars["String"];
  sysPath: Scalars["String"];
  filename: Scalars["String"];
  filetype?: Maybe<Scalars["String"]>;
  timestamp?: Maybe<Scalars["String"]>;
  uploadedBy?: Maybe<Scalars["String"]>;
};

export type Department = {
  __typename?: "Department";
  id: Scalars["String"];
  departmentName: Scalars["String"];
};

export enum GenderTEnum {
  Male = "Male",
  Female = "Female",
  Indeterminate = "Indeterminate",
  Intersex = "Intersex",
  Unspecified = "Unspecified"
}

export type JobTitle = {
  __typename?: "JobTitle";
  id: Scalars["String"];
  jobTitleName: Scalars["String"];
};

export type Location = {
  __typename?: "Location";
  id: Scalars["String"];
  locationName: Scalars["String"];
};

export type Mutation = {
  __typename?: "Mutation";
  createProjectModule?: Maybe<ProjectModule>;
  updateProjectModule?: Maybe<ProjectNote>;
  deleteProjectModule?: Maybe<ProjectNote>;
  createProject?: Maybe<Project>;
  updateProject?: Maybe<Project>;
  deleteProject?: Maybe<Project>;
  createTaskTodo?: Maybe<TaskTodo>;
  updateTaskTodoStringField?: Maybe<TaskTodo>;
  updateTaskTodoBooleanField?: Maybe<TaskTodo>;
  deleteTaskTodo?: Maybe<TaskTodo>;
  updateProjectTask?: Maybe<ProjectTask>;
  updateProjectTaskField?: Maybe<ProjectTask>;
  createProjectTask?: Maybe<ProjectTask>;
  deleteProjectTask?: Maybe<ProjectTask>;
  createUser?: Maybe<User>;
  deleteUser?: Maybe<User>;
  updateUser?: Maybe<User>;
  createJobTitle?: Maybe<JobTitle>;
  deleteJobTitle?: Maybe<JobTitle>;
  updateJobTitle?: Maybe<JobTitle>;
  createLocation?: Maybe<Location>;
  deleteLocation?: Maybe<Location>;
  updateLocation?: Maybe<Location>;
  createProjectUser?: Maybe<ProjectUser>;
  deleteProjectUser?: Maybe<ProjectUser>;
  postComment?: Maybe<TaskComment>;
  createDepartment?: Maybe<Department>;
  deleteDepartment?: Maybe<Department>;
  updateDepartment?: Maybe<Department>;
  createAttachment?: Maybe<Attachment>;
  deleteAttachment?: Maybe<Attachment>;
};

export type MutationCreateProjectModuleArgs = {
  projectType: Scalars["String"];
  moduleName?: Maybe<Scalars["String"]>;
  ownerUserId?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  statusType?: Maybe<StatusTEnum>;
};

export type MutationUpdateProjectModuleArgs = {
  id: Scalars["String"];
  noteName?: Maybe<Scalars["String"]>;
  dateNotification?: Maybe<Scalars["Float"]>;
  projectType: Scalars["String"];
};

export type MutationDeleteProjectModuleArgs = {
  id: Scalars["String"];
};

export type MutationCreateProjectArgs = {
  projectName: Scalars["String"];
  ownerUserId: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  dateCompletion?: Maybe<Scalars["String"]>;
  projectStatusId?: Maybe<Scalars["String"]>;
};

export type MutationUpdateProjectArgs = {
  id: Scalars["String"];
  projectName: Scalars["String"];
  ownerUserId: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  dateCompletion?: Maybe<Scalars["String"]>;
  projectStatusId?: Maybe<Scalars["String"]>;
};

export type MutationDeleteProjectArgs = {
  id: Scalars["String"];
};

export type MutationCreateTaskTodoArgs = {
  todo: Scalars["String"];
  taskType: Scalars["String"];
};

export type MutationUpdateTaskTodoStringFieldArgs = {
  id: Scalars["String"];
  taskType: Scalars["String"];
  field: Scalars["String"];
  value?: Maybe<Scalars["String"]>;
};

export type MutationUpdateTaskTodoBooleanFieldArgs = {
  id: Scalars["String"];
  taskType: Scalars["String"];
  field: Scalars["String"];
  value?: Maybe<Scalars["Boolean"]>;
};

export type MutationDeleteTaskTodoArgs = {
  id: Scalars["String"];
  taskType: Scalars["String"];
};

export type MutationUpdateProjectTaskArgs = {
  id: Scalars["String"];
  taskName?: Maybe<Scalars["String"]>;
  ownerUserId?: Maybe<Scalars["String"]>;
  dateStart?: Maybe<Scalars["String"]>;
  dateEnd?: Maybe<Scalars["String"]>;
  dateTarget?: Maybe<Scalars["String"]>;
  statusType?: Maybe<Scalars["String"]>;
  projectType: Scalars["String"];
};

export type MutationUpdateProjectTaskFieldArgs = {
  id: Scalars["String"];
  projectType: Scalars["String"];
  field: Scalars["String"];
  value?: Maybe<Scalars["String"]>;
};

export type MutationCreateProjectTaskArgs = {
  taskName: Scalars["String"];
  ownerUserId: Scalars["String"];
  dateStart?: Maybe<Scalars["String"]>;
  dateEnd?: Maybe<Scalars["String"]>;
  dateTarget?: Maybe<Scalars["String"]>;
  statusType?: Maybe<Scalars["String"]>;
  projectType: Scalars["String"];
};

export type MutationDeleteProjectTaskArgs = {
  id: Scalars["String"];
  projectType: Scalars["String"];
};

export type MutationCreateUserArgs = {
  id: Scalars["String"];
  firstName: Scalars["String"];
  lastName: Scalars["String"];
  workId?: Maybe<Scalars["String"]>;
  emailAddress?: Maybe<Scalars["String"]>;
  dateOfBirth?: Maybe<Scalars["String"]>;
  gender?: Maybe<GenderTEnum>;
  profilePhoto?: Maybe<Scalars["String"]>;
  homePhoneNumber?: Maybe<Scalars["String"]>;
  cellPhoneNumber?: Maybe<Scalars["String"]>;
  address?: Maybe<Scalars["String"]>;
  city?: Maybe<Scalars["String"]>;
  suburb?: Maybe<Scalars["String"]>;
  countryState?: Maybe<Scalars["String"]>;
  postcode?: Maybe<Scalars["String"]>;
  tfn?: Maybe<Scalars["String"]>;
  bankName?: Maybe<Scalars["String"]>;
  branch?: Maybe<Scalars["String"]>;
  accountName?: Maybe<Scalars["String"]>;
  bsb?: Maybe<Scalars["String"]>;
  accountNumber?: Maybe<Scalars["String"]>;
  emergencyContactName?: Maybe<Scalars["String"]>;
  emergencyContactNumber?: Maybe<Scalars["String"]>;
  emergencyContactAddress?: Maybe<Scalars["String"]>;
  emergencyContactCity?: Maybe<Scalars["String"]>;
  emergencyContactSuburb?: Maybe<Scalars["String"]>;
  emergencyContactState?: Maybe<Scalars["String"]>;
  emergencyContactPostcode?: Maybe<Scalars["String"]>;
  emergencyContactRelationship?: Maybe<Scalars["String"]>;
  workStartDate?: Maybe<Scalars["String"]>;
  workTitleType?: Maybe<Scalars["String"]>;
  workPhoneNumber?: Maybe<Scalars["String"]>;
  workCellphoneNumber?: Maybe<Scalars["String"]>;
  workDepartmentType?: Maybe<Scalars["String"]>;
  workLocationType?: Maybe<Scalars["String"]>;
  workEmailAddress?: Maybe<Scalars["String"]>;
};

export type MutationDeleteUserArgs = {
  id: Scalars["String"];
};

export type MutationUpdateUserArgs = {
  id: Scalars["String"];
  firstName: Scalars["String"];
  lastName: Scalars["String"];
  workId?: Maybe<Scalars["String"]>;
  emailAddress?: Maybe<Scalars["String"]>;
  dateOfBirth?: Maybe<Scalars["String"]>;
  gender?: Maybe<GenderTEnum>;
  profilePhoto?: Maybe<Scalars["String"]>;
  homePhoneNumber?: Maybe<Scalars["String"]>;
  cellPhoneNumber?: Maybe<Scalars["String"]>;
  address?: Maybe<Scalars["String"]>;
  city?: Maybe<Scalars["String"]>;
  suburb?: Maybe<Scalars["String"]>;
  countryState?: Maybe<Scalars["String"]>;
  postcode?: Maybe<Scalars["String"]>;
  tfn?: Maybe<Scalars["String"]>;
  bankName?: Maybe<Scalars["String"]>;
  branch?: Maybe<Scalars["String"]>;
  accountName?: Maybe<Scalars["String"]>;
  bsb?: Maybe<Scalars["String"]>;
  accountNumber?: Maybe<Scalars["String"]>;
  emergencyContactName?: Maybe<Scalars["String"]>;
  emergencyContactNumber?: Maybe<Scalars["String"]>;
  emergencyContactAddress?: Maybe<Scalars["String"]>;
  emergencyContactCity?: Maybe<Scalars["String"]>;
  emergencyContactSuburb?: Maybe<Scalars["String"]>;
  emergencyContactState?: Maybe<Scalars["String"]>;
  emergencyContactPostcode?: Maybe<Scalars["String"]>;
  emergencyContactRelationship?: Maybe<Scalars["String"]>;
  workStartDate?: Maybe<Scalars["String"]>;
  workTitleType?: Maybe<Scalars["String"]>;
  workPhoneNumber?: Maybe<Scalars["String"]>;
  workCellphoneNumber?: Maybe<Scalars["String"]>;
  workDepartmentType?: Maybe<Scalars["String"]>;
  workLocationType?: Maybe<Scalars["String"]>;
  workEmailAddress?: Maybe<Scalars["String"]>;
};

export type MutationCreateJobTitleArgs = {
  jobTitleName: Scalars["String"];
};

export type MutationDeleteJobTitleArgs = {
  id: Scalars["String"];
};

export type MutationUpdateJobTitleArgs = {
  id: Scalars["String"];
  titleName: Scalars["String"];
};

export type MutationCreateLocationArgs = {
  locationName: Scalars["String"];
};

export type MutationDeleteLocationArgs = {
  id: Scalars["String"];
};

export type MutationUpdateLocationArgs = {
  id: Scalars["String"];
  locationName: Scalars["String"];
};

export type MutationCreateProjectUserArgs = {
  userId: Scalars["String"];
  projectType: Scalars["String"];
};

export type MutationDeleteProjectUserArgs = {
  id: Scalars["String"];
  projectType: Scalars["String"];
};

export type MutationPostCommentArgs = {
  taskType: Scalars["String"];
  postMessage: Scalars["String"];
};

export type MutationCreateDepartmentArgs = {
  departmentName: Scalars["String"];
};

export type MutationDeleteDepartmentArgs = {
  id: Scalars["String"];
};

export type MutationUpdateDepartmentArgs = {
  id: Scalars["String"];
  departmentName: Scalars["String"];
};

export type MutationCreateAttachmentArgs = {
  sysPath: Scalars["String"];
  filename: Scalars["String"];
  filetype?: Maybe<Scalars["String"]>;
};

export type MutationDeleteAttachmentArgs = {
  id: Scalars["String"];
  sysPath: Scalars["String"];
};

export type Project = {
  __typename?: "Project";
  id: Scalars["String"];
  projectName?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  dateCompletion?: Maybe<Scalars["String"]>;
  ownerUserId?: Maybe<Scalars["String"]>;
  projectStatusId?: Maybe<Scalars["String"]>;
  projectModuleList?: Maybe<Array<Maybe<ProjectModule>>>;
  projectNoteList?: Maybe<Array<Maybe<ProjectNote>>>;
  projectUserList?: Maybe<Array<Maybe<ProjectUser>>>;
  projectTaskList?: Maybe<Array<Maybe<ProjectTask>>>;
};

export type ProjectLookup = {
  __typename?: "ProjectLookup";
  Users?: Maybe<Array<Maybe<User>>>;
};

export type ProjectModule = {
  __typename?: "ProjectModule";
  id: Scalars["String"];
  moduleName?: Maybe<Scalars["String"]>;
  ownerUserId?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  statusType?: Maybe<StatusTEnum>;
  projectType?: Maybe<Project>;
};

export type ProjectNote = {
  __typename?: "ProjectNote";
  id: Scalars["String"];
  noteName?: Maybe<Scalars["String"]>;
  dateNotification?: Maybe<Scalars["String"]>;
  projectType?: Maybe<Project>;
};

export type ProjectTask = {
  __typename?: "ProjectTask";
  id: Scalars["String"];
  taskName?: Maybe<Scalars["String"]>;
  ownerUserId?: Maybe<Scalars["String"]>;
  dateStart?: Maybe<Scalars["String"]>;
  dateEnd?: Maybe<Scalars["String"]>;
  dateTarget?: Maybe<Scalars["String"]>;
  statusType?: Maybe<Scalars["String"]>;
  testUserId?: Maybe<Scalars["String"]>;
  priority?: Maybe<Scalars["String"]>;
  projectType?: Maybe<Project>;
};

export type ProjectUser = {
  __typename?: "ProjectUser";
  id: Scalars["String"];
  userId: Scalars["String"];
  accepted?: Maybe<Scalars["Boolean"]>;
  projectType?: Maybe<Project>;
};

export type Query = {
  __typename?: "Query";
  viewProjectTask?: Maybe<ProjectTask>;
  listProjectTaskByProject?: Maybe<Array<Maybe<ProjectTask>>>;
  listProjectTask?: Maybe<Array<Maybe<ProjectTask>>>;
  viewProject?: Maybe<Project>;
  listProjects?: Maybe<Array<Maybe<Project>>>;
  listDepartments?: Maybe<Array<Maybe<Department>>>;
  viewDepartment?: Maybe<Department>;
  viewTaskTodo?: Maybe<TaskTodo>;
  listTaskTodoByTask?: Maybe<Array<Maybe<TaskTodo>>>;
  listTaskTodo?: Maybe<Array<Maybe<TaskTodo>>>;
  listJobTitles?: Maybe<Array<Maybe<JobTitle>>>;
  viewJobTitle?: Maybe<JobTitle>;
  statusTypes?: Maybe<Array<Maybe<TaskStatus>>>;
  viewProjectNote?: Maybe<ProjectNote>;
  listProjectNotes?: Maybe<Array<Maybe<ProjectNote>>>;
  listLocations?: Maybe<Array<Maybe<Location>>>;
  viewLocation?: Maybe<Location>;
  projectLookup?: Maybe<ProjectLookup>;
  userLookup?: Maybe<UserLookup>;
  getTaskCommentsByTask?: Maybe<Array<Maybe<TaskComment>>>;
  viewUser?: Maybe<User>;
  listUsers?: Maybe<Array<Maybe<User>>>;
  getAllAttachmentBySysPath?: Maybe<Array<Maybe<Attachment>>>;
  getAllAttachments?: Maybe<Array<Maybe<Attachment>>>;
};

export type QueryViewProjectTaskArgs = {
  id: Scalars["String"];
  projectType: Scalars["String"];
};

export type QueryListProjectTaskByProjectArgs = {
  projectType: Scalars["String"];
};

export type QueryViewProjectArgs = {
  id: Scalars["String"];
};

export type QueryViewDepartmentArgs = {
  id: Scalars["String"];
};

export type QueryViewTaskTodoArgs = {
  id: Scalars["String"];
  taskType: Scalars["String"];
};

export type QueryListTaskTodoByTaskArgs = {
  taskType: Scalars["String"];
};

export type QueryViewJobTitleArgs = {
  id: Scalars["String"];
};

export type QueryViewProjectNoteArgs = {
  id: Scalars["String"];
  projectType: Scalars["String"];
};

export type QueryViewLocationArgs = {
  id: Scalars["String"];
};

export type QueryGetTaskCommentsByTaskArgs = {
  taskType: Scalars["String"];
};

export type QueryViewUserArgs = {
  id: Scalars["String"];
};

export type QueryGetAllAttachmentBySysPathArgs = {
  sysPath: Scalars["String"];
};

export enum StatusTEnum {
  /** In Progress */
  InProgress = "InProgress",
  /** On Going */
  OnGoing = "OnGoing",
  /** Completed */
  Completed = "Completed",
  /** Cancelled */
  Cancelled = "Cancelled"
}

export type TaskComment = {
  __typename?: "TaskComment";
  id: Scalars["String"];
  userType?: Maybe<Scalars["String"]>;
  postMessage: Scalars["String"];
  datePosted: Scalars["String"];
};

export type TaskStatus = {
  __typename?: "TaskStatus";
  id: Scalars["String"];
  text: Scalars["String"];
};

export type TaskTodo = {
  __typename?: "TaskTodo";
  id: Scalars["String"];
  todo: Scalars["String"];
  dateCreated?: Maybe<Scalars["String"]>;
  isCompleted?: Maybe<Scalars["Boolean"]>;
  taskType?: Maybe<Scalars["String"]>;
  statusType?: Maybe<Scalars["String"]>;
};

export type User = {
  __typename?: "User";
  id: Scalars["String"];
  firstName: Scalars["String"];
  lastName: Scalars["String"];
  emailAddress?: Maybe<Scalars["String"]>;
  dateOfBirth?: Maybe<Scalars["String"]>;
  gender?: Maybe<GenderTEnum>;
  profilePhoto?: Maybe<Scalars["String"]>;
  homePhoneNumber?: Maybe<Scalars["String"]>;
  cellPhoneNumber?: Maybe<Scalars["String"]>;
  address?: Maybe<Scalars["String"]>;
  city?: Maybe<Scalars["String"]>;
  suburb?: Maybe<Scalars["String"]>;
  countryState?: Maybe<Scalars["String"]>;
  postcode?: Maybe<Scalars["String"]>;
  tfn?: Maybe<Scalars["String"]>;
  bankName?: Maybe<Scalars["String"]>;
  branch?: Maybe<Scalars["String"]>;
  accountName?: Maybe<Scalars["String"]>;
  bsb?: Maybe<Scalars["String"]>;
  accountNumber?: Maybe<Scalars["String"]>;
  emergencyContactName?: Maybe<Scalars["String"]>;
  emergencyContactNumber?: Maybe<Scalars["String"]>;
  emergencyContactAddress?: Maybe<Scalars["String"]>;
  emergencyContactCity?: Maybe<Scalars["String"]>;
  emergencyContactSuburb?: Maybe<Scalars["String"]>;
  emergencyContactState?: Maybe<Scalars["String"]>;
  emergencyContactPostcode?: Maybe<Scalars["String"]>;
  emergencyContactRelationship?: Maybe<Scalars["String"]>;
  workId?: Maybe<Scalars["String"]>;
  workStartDate?: Maybe<Scalars["String"]>;
  workTitleType?: Maybe<JobTitle>;
  workPhoneNumber?: Maybe<Scalars["String"]>;
  workCellphoneNumber?: Maybe<Scalars["String"]>;
  workDepartmentType?: Maybe<Department>;
  workLocationType?: Maybe<Location>;
  workEmailAddress?: Maybe<Scalars["String"]>;
};

export type UserLookup = {
  __typename?: "UserLookup";
  JobTitles?: Maybe<Array<Maybe<JobTitle>>>;
  Departments?: Maybe<Array<Maybe<Department>>>;
  Locations?: Maybe<Array<Maybe<Location>>>;
};
export type GetAttachmentsBySysPathQueryVariables = {
  sysPath: Scalars["String"];
};

export type GetAttachmentsBySysPathQuery = { __typename?: "Query" } & {
  getAllAttachmentBySysPath: Maybe<
    Array<
      Maybe<
        { __typename?: "Attachment" } & Pick<
          Attachment,
          | "id"
          | "sysPath"
          | "filename"
          | "filetype"
          | "timestamp"
          | "uploadedBy"
        >
      >
    >
  >;
};

export type CreateAttachmentMutationVariables = {
  sysPath: Scalars["String"];
  filename: Scalars["String"];
  filetype: Scalars["String"];
};

export type CreateAttachmentMutation = { __typename?: "Mutation" } & {
  createAttachment: Maybe<
    { __typename?: "Attachment" } & Pick<
      Attachment,
      "id" | "sysPath" | "filename" | "filetype" | "timestamp" | "uploadedBy"
    >
  >;
};

export type DeleteAttachmentMutationVariables = {
  id: Scalars["String"];
  sysPath: Scalars["String"];
};

export type DeleteAttachmentMutation = { __typename?: "Mutation" } & {
  deleteAttachment: Maybe<
    { __typename?: "Attachment" } & Pick<Attachment, "id">
  >;
};

export type ListDepartmentsQueryVariables = {};

export type ListDepartmentsQuery = { __typename?: "Query" } & {
  listDepartments: Maybe<
    Array<
      Maybe<
        { __typename?: "Department" } & Pick<
          Department,
          "id" | "departmentName"
        >
      >
    >
  >;
};

export type ViewDepartmentQueryVariables = {
  id: Scalars["String"];
};

export type ViewDepartmentQuery = { __typename?: "Query" } & {
  viewDepartment: Maybe<
    { __typename?: "Department" } & Pick<Department, "id" | "departmentName">
  >;
};

export type CreateDepartmentMutationVariables = {
  departmentName: Scalars["String"];
};

export type CreateDepartmentMutation = { __typename?: "Mutation" } & {
  createDepartment: Maybe<
    { __typename?: "Department" } & Pick<Department, "id" | "departmentName">
  >;
};

export type DeleteDepartmentMutationVariables = {
  id: Scalars["String"];
};

export type DeleteDepartmentMutation = { __typename?: "Mutation" } & {
  deleteDepartment: Maybe<
    { __typename?: "Department" } & Pick<Department, "id">
  >;
};

export type UpdateDepartmentMutationVariables = {
  id: Scalars["String"];
  departmentName: Scalars["String"];
};

export type UpdateDepartmentMutation = { __typename?: "Mutation" } & {
  updateDepartment: Maybe<
    { __typename?: "Department" } & Pick<Department, "id" | "departmentName">
  >;
};

export type ListJobTitlesQueryVariables = {};

export type ListJobTitlesQuery = { __typename?: "Query" } & {
  listJobTitles: Maybe<
    Array<
      Maybe<{ __typename?: "JobTitle" } & Pick<JobTitle, "id" | "jobTitleName">>
    >
  >;
};

export type CreateJobTitleMutationVariables = {
  jobTitleName: Scalars["String"];
};

export type CreateJobTitleMutation = { __typename?: "Mutation" } & {
  createJobTitle: Maybe<
    { __typename?: "JobTitle" } & Pick<JobTitle, "id" | "jobTitleName">
  >;
};

export type DeleteJobTitleMutationVariables = {
  id: Scalars["String"];
};

export type DeleteJobTitleMutation = { __typename?: "Mutation" } & {
  deleteJobTitle: Maybe<{ __typename?: "JobTitle" } & Pick<JobTitle, "id">>;
};

export type ListLocationsQueryVariables = {};

export type ListLocationsQuery = { __typename?: "Query" } & {
  listLocations: Maybe<
    Array<
      Maybe<{ __typename?: "Location" } & Pick<Location, "id" | "locationName">>
    >
  >;
};

export type ViewLocationQueryVariables = {
  id: Scalars["String"];
};

export type ViewLocationQuery = { __typename?: "Query" } & {
  viewLocation: Maybe<
    { __typename?: "Location" } & Pick<Location, "id" | "locationName">
  >;
};

export type CreateLocationMutationVariables = {
  locationName: Scalars["String"];
};

export type CreateLocationMutation = { __typename?: "Mutation" } & {
  createLocation: Maybe<
    { __typename?: "Location" } & Pick<Location, "id" | "locationName">
  >;
};

export type UpdateLocationMutationVariables = {
  id: Scalars["String"];
  locationName: Scalars["String"];
};

export type UpdateLocationMutation = { __typename?: "Mutation" } & {
  updateLocation: Maybe<
    { __typename?: "Location" } & Pick<Location, "id" | "locationName">
  >;
};

export type DeleteLocationMutationVariables = {
  id: Scalars["String"];
};

export type DeleteLocationMutation = { __typename?: "Mutation" } & {
  deleteLocation: Maybe<{ __typename?: "Location" } & Pick<Location, "id">>;
};

export type UserLookupQueryVariables = {};

export type UserLookupQuery = { __typename?: "Query" } & {
  userLookup: Maybe<
    { __typename?: "UserLookup" } & {
      JobTitles: Maybe<
        Array<
          Maybe<
            { __typename?: "JobTitle" } & Pick<JobTitle, "id" | "jobTitleName">
          >
        >
      >;
      Departments: Maybe<
        Array<
          Maybe<
            { __typename?: "Department" } & Pick<
              Department,
              "id" | "departmentName"
            >
          >
        >
      >;
      Locations: Maybe<
        Array<
          Maybe<
            { __typename?: "Location" } & Pick<Location, "id" | "locationName">
          >
        >
      >;
    }
  >;
};

export type ProjectLookupQueryVariables = {};

export type ProjectLookupQuery = { __typename?: "Query" } & {
  projectLookup: Maybe<
    { __typename?: "ProjectLookup" } & {
      Users: Maybe<
        Array<
          Maybe<
            { __typename?: "User" } & Pick<
              User,
              "id" | "firstName" | "lastName" | "emailAddress" | "profilePhoto"
            >
          >
        >
      >;
    }
  >;
};

export type StatusTypesQueryVariables = {};

export type StatusTypesQuery = { __typename?: "Query" } & {
  statusTypes: Maybe<
    Array<
      Maybe<{ __typename?: "TaskStatus" } & Pick<TaskStatus, "id" | "text">>
    >
  >;
};

export type ListProjectsQueryVariables = {};

export type ListProjectsQuery = { __typename?: "Query" } & {
  listProjects: Maybe<
    Array<
      Maybe<
        { __typename?: "Project" } & Pick<
          Project,
          | "id"
          | "projectName"
          | "description"
          | "dateCompletion"
          | "ownerUserId"
          | "projectStatusId"
        > & {
            projectModuleList: Maybe<
              Array<
                Maybe<
                  { __typename?: "ProjectModule" } & Pick<
                    ProjectModule,
                    "id" | "moduleName"
                  >
                >
              >
            >;
            projectNoteList: Maybe<
              Array<
                Maybe<
                  { __typename?: "ProjectNote" } & Pick<
                    ProjectNote,
                    "id" | "noteName"
                  >
                >
              >
            >;
            projectUserList: Maybe<
              Array<
                Maybe<
                  { __typename?: "ProjectUser" } & Pick<
                    ProjectUser,
                    "id" | "userId" | "accepted"
                  >
                >
              >
            >;
          }
      >
    >
  >;
};

export type CreateProjectMutationVariables = {
  projectName: Scalars["String"];
  ownerUserId: Scalars["String"];
  description: Scalars["String"];
};

export type CreateProjectMutation = { __typename?: "Mutation" } & {
  createProject: Maybe<
    { __typename?: "Project" } & Pick<
      Project,
      "id" | "ownerUserId" | "projectName" | "description" | "dateCompletion"
    >
  >;
};

export type DeleteProjectMutationVariables = {
  id: Scalars["String"];
};

export type DeleteProjectMutation = { __typename?: "Mutation" } & {
  deleteProject: Maybe<{ __typename?: "Project" } & Pick<Project, "id">>;
};

export type ListTasksQueryVariables = {};

export type ListTasksQuery = { __typename?: "Query" } & {
  listProjectTask: Maybe<
    Array<
      Maybe<
        { __typename?: "ProjectTask" } & Pick<
          ProjectTask,
          | "id"
          | "taskName"
          | "ownerUserId"
          | "dateStart"
          | "dateEnd"
          | "dateTarget"
          | "statusType"
          | "priority"
        > & {
            projectType: Maybe<
              { __typename?: "Project" } & Pick<Project, "id" | "projectName">
            >;
          }
      >
    >
  >;
};

export type ListProjectTaskByProjectQueryVariables = {
  projectType: Scalars["String"];
};

export type ListProjectTaskByProjectQuery = { __typename?: "Query" } & {
  listProjectTaskByProject: Maybe<
    Array<
      Maybe<
        { __typename?: "ProjectTask" } & Pick<
          ProjectTask,
          | "id"
          | "taskName"
          | "dateStart"
          | "dateEnd"
          | "dateTarget"
          | "statusType"
          | "priority"
        > & {
            projectType: Maybe<
              { __typename?: "Project" } & Pick<Project, "id">
            >;
          }
      >
    >
  >;
};

export type ViewProjectTaskQueryVariables = {
  taskId: Scalars["String"];
  projectType: Scalars["String"];
};

export type ViewProjectTaskQuery = { __typename?: "Query" } & {
  viewProjectTask: Maybe<
    { __typename?: "ProjectTask" } & Pick<
      ProjectTask,
      | "id"
      | "taskName"
      | "ownerUserId"
      | "dateStart"
      | "dateEnd"
      | "dateTarget"
      | "statusType"
      | "testUserId"
      | "priority"
    > & { projectType: Maybe<{ __typename?: "Project" } & Pick<Project, "id">> }
  >;
};

export type CreateProjectTaskMutationVariables = {
  taskName: Scalars["String"];
  ownerUserId: Scalars["String"];
  projectType: Scalars["String"];
  dateStart?: Maybe<Scalars["String"]>;
  dateEnd?: Maybe<Scalars["String"]>;
  statusType?: Maybe<Scalars["String"]>;
};

export type CreateProjectTaskMutation = { __typename?: "Mutation" } & {
  createProjectTask: Maybe<
    { __typename?: "ProjectTask" } & Pick<ProjectTask, "taskName">
  >;
};

export type DeleteProjectTaskMutationVariables = {
  id: Scalars["String"];
  projectType: Scalars["String"];
};

export type DeleteProjectTaskMutation = { __typename?: "Mutation" } & {
  deleteProjectTask: Maybe<
    { __typename?: "ProjectTask" } & Pick<ProjectTask, "id">
  >;
};

export type UpdateDateStartMutationVariables = {
  taskId: Scalars["String"];
  projectType: Scalars["String"];
  dateStart: Scalars["String"];
};

export type UpdateDateStartMutation = { __typename?: "Mutation" } & {
  updateProjectTask: Maybe<
    { __typename?: "ProjectTask" } & Pick<ProjectTask, "id" | "dateStart">
  >;
};

export type UpdateProjectTaskFieldMutationVariables = {
  taskId: Scalars["String"];
  projectType: Scalars["String"];
  field: Scalars["String"];
  value?: Maybe<Scalars["String"]>;
};

export type UpdateProjectTaskFieldMutation = { __typename?: "Mutation" } & {
  updateProjectTaskField: Maybe<
    { __typename?: "ProjectTask" } & Pick<ProjectTask, "id">
  >;
};

export type PostCommentMutationVariables = {
  taskType: Scalars["String"];
  postMessage: Scalars["String"];
};

export type PostCommentMutation = { __typename?: "Mutation" } & {
  postComment: Maybe<
    { __typename?: "TaskComment" } & Pick<
      TaskComment,
      "id" | "userType" | "datePosted" | "postMessage"
    >
  >;
};

export type GetTaskCommentsByTaskQueryVariables = {
  taskType: Scalars["String"];
};

export type GetTaskCommentsByTaskQuery = { __typename?: "Query" } & {
  getTaskCommentsByTask: Maybe<
    Array<
      Maybe<
        { __typename?: "TaskComment" } & Pick<
          TaskComment,
          "id" | "userType" | "postMessage" | "datePosted"
        >
      >
    >
  >;
};

export type ListTaskTodoByTaskQueryVariables = {
  taskType: Scalars["String"];
};

export type ListTaskTodoByTaskQuery = { __typename?: "Query" } & {
  listTaskTodoByTask: Maybe<
    Array<
      Maybe<
        { __typename?: "TaskTodo" } & Pick<
          TaskTodo,
          "id" | "todo" | "dateCreated" | "isCompleted" | "taskType"
        >
      >
    >
  >;
};

export type UpdateTaskTodoStringFieldMutationVariables = {
  taskId: Scalars["String"];
  id: Scalars["String"];
  field: Scalars["String"];
  value: Scalars["String"];
};

export type UpdateTaskTodoStringFieldMutation = { __typename?: "Mutation" } & {
  updateTaskTodoStringField: Maybe<
    { __typename?: "TaskTodo" } & Pick<TaskTodo, "id">
  >;
};

export type UpdateTaskTodoBooleanFieldMutationVariables = {
  taskId: Scalars["String"];
  id: Scalars["String"];
  field: Scalars["String"];
  value: Scalars["Boolean"];
};

export type UpdateTaskTodoBooleanFieldMutation = { __typename?: "Mutation" } & {
  updateTaskTodoBooleanField: Maybe<
    { __typename?: "TaskTodo" } & Pick<TaskTodo, "id">
  >;
};

export type CreateTaskTodoMutationVariables = {
  taskType: Scalars["String"];
  todo: Scalars["String"];
};

export type CreateTaskTodoMutation = { __typename?: "Mutation" } & {
  createTaskTodo: Maybe<{ __typename?: "TaskTodo" } & Pick<TaskTodo, "id">>;
};

export type DeleteTaskTodoMutationVariables = {
  taskType: Scalars["String"];
  id: Scalars["String"];
};

export type DeleteTaskTodoMutation = { __typename?: "Mutation" } & {
  deleteTaskTodo: Maybe<{ __typename?: "TaskTodo" } & Pick<TaskTodo, "id">>;
};

export type ListUsersQueryVariables = {};

export type ListUsersQuery = { __typename?: "Query" } & {
  listUsers: Maybe<
    Array<
      Maybe<
        { __typename?: "User" } & Pick<
          User,
          "id" | "firstName" | "lastName" | "emailAddress" | "workId"
        >
      >
    >
  >;
};

export type ViewUserQueryVariables = {
  id: Scalars["String"];
};

export type ViewUserQuery = { __typename?: "Query" } & {
  viewUser: Maybe<
    { __typename?: "User" } & Pick<
      User,
      | "id"
      | "firstName"
      | "lastName"
      | "emailAddress"
      | "dateOfBirth"
      | "gender"
      | "profilePhoto"
      | "homePhoneNumber"
      | "cellPhoneNumber"
      | "address"
      | "city"
      | "suburb"
      | "countryState"
      | "postcode"
      | "tfn"
      | "bankName"
      | "branch"
      | "accountName"
      | "bsb"
      | "accountNumber"
      | "emergencyContactName"
      | "emergencyContactNumber"
      | "emergencyContactAddress"
      | "emergencyContactCity"
      | "emergencyContactSuburb"
      | "emergencyContactState"
      | "emergencyContactPostcode"
      | "emergencyContactRelationship"
      | "workId"
      | "workStartDate"
      | "workPhoneNumber"
      | "workCellphoneNumber"
      | "workEmailAddress"
    > & {
        workTitleType: Maybe<
          { __typename?: "JobTitle" } & Pick<JobTitle, "id" | "jobTitleName">
        >;
        workDepartmentType: Maybe<
          { __typename?: "Department" } & Pick<
            Department,
            "id" | "departmentName"
          >
        >;
        workLocationType: Maybe<
          { __typename?: "Location" } & Pick<Location, "id" | "locationName">
        >;
      }
  >;
};

export type CreateUserMutationVariables = {
  id: Scalars["String"];
  firstName: Scalars["String"];
  lastName: Scalars["String"];
};

export type CreateUserMutation = { __typename?: "Mutation" } & {
  createUser: Maybe<
    { __typename?: "User" } & Pick<User, "id" | "firstName" | "lastName">
  >;
};

export type DeleteUserMutationVariables = {
  id: Scalars["String"];
};

export type DeleteUserMutation = { __typename?: "Mutation" } & {
  deleteUser: Maybe<{ __typename?: "User" } & Pick<User, "id">>;
};

export type UpdateUserMutationVariables = {
  id: Scalars["String"];
  firstName: Scalars["String"];
  lastName: Scalars["String"];
  emailAddress?: Maybe<Scalars["String"]>;
  dateOfBirth?: Maybe<Scalars["String"]>;
  gender?: Maybe<GenderTEnum>;
  profilePhoto?: Maybe<Scalars["String"]>;
  homePhoneNumber?: Maybe<Scalars["String"]>;
  cellPhoneNumber?: Maybe<Scalars["String"]>;
  address?: Maybe<Scalars["String"]>;
  city?: Maybe<Scalars["String"]>;
  suburb?: Maybe<Scalars["String"]>;
  countryState?: Maybe<Scalars["String"]>;
  postcode?: Maybe<Scalars["String"]>;
  tfn?: Maybe<Scalars["String"]>;
  bankName?: Maybe<Scalars["String"]>;
  branch?: Maybe<Scalars["String"]>;
  accountName?: Maybe<Scalars["String"]>;
  bsb?: Maybe<Scalars["String"]>;
  accountNumber?: Maybe<Scalars["String"]>;
  emergencyContactName?: Maybe<Scalars["String"]>;
  emergencyContactNumber?: Maybe<Scalars["String"]>;
  emergencyContactAddress?: Maybe<Scalars["String"]>;
  emergencyContactCity?: Maybe<Scalars["String"]>;
  emergencyContactSuburb?: Maybe<Scalars["String"]>;
  emergencyContactState?: Maybe<Scalars["String"]>;
  emergencyContactPostcode?: Maybe<Scalars["String"]>;
  emergencyContactRelationship?: Maybe<Scalars["String"]>;
  workId?: Maybe<Scalars["String"]>;
  workStartDate?: Maybe<Scalars["String"]>;
  workTitleType?: Maybe<Scalars["String"]>;
  workPhoneNumber?: Maybe<Scalars["String"]>;
  workCellphoneNumber?: Maybe<Scalars["String"]>;
  workDepartmentType?: Maybe<Scalars["String"]>;
  workLocationType?: Maybe<Scalars["String"]>;
  workEmailAddress?: Maybe<Scalars["String"]>;
};

export type UpdateUserMutation = { __typename?: "Mutation" } & {
  updateUser: Maybe<
    { __typename?: "User" } & Pick<
      User,
      | "id"
      | "firstName"
      | "lastName"
      | "emailAddress"
      | "dateOfBirth"
      | "gender"
      | "profilePhoto"
      | "homePhoneNumber"
      | "cellPhoneNumber"
      | "address"
      | "city"
      | "suburb"
      | "countryState"
      | "postcode"
      | "tfn"
      | "bankName"
      | "branch"
      | "accountName"
      | "bsb"
      | "accountNumber"
      | "emergencyContactName"
      | "emergencyContactNumber"
      | "emergencyContactAddress"
      | "emergencyContactCity"
      | "emergencyContactSuburb"
      | "emergencyContactState"
      | "emergencyContactPostcode"
      | "emergencyContactRelationship"
      | "workId"
      | "workStartDate"
      | "workPhoneNumber"
      | "workCellphoneNumber"
      | "workEmailAddress"
    > & {
        workTitleType: Maybe<
          { __typename?: "JobTitle" } & Pick<JobTitle, "id" | "jobTitleName">
        >;
        workDepartmentType: Maybe<
          { __typename?: "Department" } & Pick<
            Department,
            "id" | "departmentName"
          >
        >;
        workLocationType: Maybe<
          { __typename?: "Location" } & Pick<Location, "id" | "locationName">
        >;
      }
  >;
};

export const GetAttachmentsBySysPathDocument = gql`
  query GetAttachmentsBySysPath($sysPath: String!) {
    getAllAttachmentBySysPath(sysPath: $sysPath) {
      id
      sysPath
      filename
      filetype
      timestamp
      uploadedBy
    }
  }
`;

export function useGetAttachmentsBySysPathQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<
    GetAttachmentsBySysPathQueryVariables
  >
) {
  return ReactApolloHooks.useQuery<
    GetAttachmentsBySysPathQuery,
    GetAttachmentsBySysPathQueryVariables
  >(GetAttachmentsBySysPathDocument, baseOptions);
}
export type GetAttachmentsBySysPathQueryHookResult = ReturnType<
  typeof useGetAttachmentsBySysPathQuery
>;
export const CreateAttachmentDocument = gql`
  mutation CreateAttachment(
    $sysPath: String!
    $filename: String!
    $filetype: String!
  ) {
    createAttachment(
      sysPath: $sysPath
      filename: $filename
      filetype: $filetype
    ) {
      id
      sysPath
      filename
      filetype
      timestamp
      uploadedBy
    }
  }
`;

export function useCreateAttachmentMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateAttachmentMutation,
    CreateAttachmentMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateAttachmentMutation,
    CreateAttachmentMutationVariables
  >(CreateAttachmentDocument, baseOptions);
}
export type CreateAttachmentMutationHookResult = ReturnType<
  typeof useCreateAttachmentMutation
>;
export const DeleteAttachmentDocument = gql`
  mutation DeleteAttachment($id: String!, $sysPath: String!) {
    deleteAttachment(id: $id, sysPath: $sysPath) {
      id
    }
  }
`;

export function useDeleteAttachmentMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    DeleteAttachmentMutation,
    DeleteAttachmentMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    DeleteAttachmentMutation,
    DeleteAttachmentMutationVariables
  >(DeleteAttachmentDocument, baseOptions);
}
export type DeleteAttachmentMutationHookResult = ReturnType<
  typeof useDeleteAttachmentMutation
>;
export const ListDepartmentsDocument = gql`
  query ListDepartments {
    listDepartments {
      id
      departmentName
    }
  }
`;

export function useListDepartmentsQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ListDepartmentsQueryVariables>
) {
  return ReactApolloHooks.useQuery<
    ListDepartmentsQuery,
    ListDepartmentsQueryVariables
  >(ListDepartmentsDocument, baseOptions);
}
export type ListDepartmentsQueryHookResult = ReturnType<
  typeof useListDepartmentsQuery
>;
export const ViewDepartmentDocument = gql`
  query ViewDepartment($id: String!) {
    viewDepartment(id: $id) {
      id
      departmentName
    }
  }
`;

export function useViewDepartmentQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ViewDepartmentQueryVariables>
) {
  return ReactApolloHooks.useQuery<
    ViewDepartmentQuery,
    ViewDepartmentQueryVariables
  >(ViewDepartmentDocument, baseOptions);
}
export type ViewDepartmentQueryHookResult = ReturnType<
  typeof useViewDepartmentQuery
>;
export const CreateDepartmentDocument = gql`
  mutation CreateDepartment($departmentName: String!) {
    createDepartment(departmentName: $departmentName) {
      id
      departmentName
    }
  }
`;

export function useCreateDepartmentMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateDepartmentMutation,
    CreateDepartmentMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateDepartmentMutation,
    CreateDepartmentMutationVariables
  >(CreateDepartmentDocument, baseOptions);
}
export type CreateDepartmentMutationHookResult = ReturnType<
  typeof useCreateDepartmentMutation
>;
export const DeleteDepartmentDocument = gql`
  mutation DeleteDepartment($id: String!) {
    deleteDepartment(id: $id) {
      id
    }
  }
`;

export function useDeleteDepartmentMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    DeleteDepartmentMutation,
    DeleteDepartmentMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    DeleteDepartmentMutation,
    DeleteDepartmentMutationVariables
  >(DeleteDepartmentDocument, baseOptions);
}
export type DeleteDepartmentMutationHookResult = ReturnType<
  typeof useDeleteDepartmentMutation
>;
export const UpdateDepartmentDocument = gql`
  mutation UpdateDepartment($id: String!, $departmentName: String!) {
    updateDepartment(id: $id, departmentName: $departmentName) {
      id
      departmentName
    }
  }
`;

export function useUpdateDepartmentMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateDepartmentMutation,
    UpdateDepartmentMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateDepartmentMutation,
    UpdateDepartmentMutationVariables
  >(UpdateDepartmentDocument, baseOptions);
}
export type UpdateDepartmentMutationHookResult = ReturnType<
  typeof useUpdateDepartmentMutation
>;
export const ListJobTitlesDocument = gql`
  query ListJobTitles {
    listJobTitles {
      id
      jobTitleName
    }
  }
`;

export function useListJobTitlesQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ListJobTitlesQueryVariables>
) {
  return ReactApolloHooks.useQuery<
    ListJobTitlesQuery,
    ListJobTitlesQueryVariables
  >(ListJobTitlesDocument, baseOptions);
}
export type ListJobTitlesQueryHookResult = ReturnType<
  typeof useListJobTitlesQuery
>;
export const CreateJobTitleDocument = gql`
  mutation CreateJobTitle($jobTitleName: String!) {
    createJobTitle(jobTitleName: $jobTitleName) {
      id
      jobTitleName
    }
  }
`;

export function useCreateJobTitleMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateJobTitleMutation,
    CreateJobTitleMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateJobTitleMutation,
    CreateJobTitleMutationVariables
  >(CreateJobTitleDocument, baseOptions);
}
export type CreateJobTitleMutationHookResult = ReturnType<
  typeof useCreateJobTitleMutation
>;
export const DeleteJobTitleDocument = gql`
  mutation DeleteJobTitle($id: String!) {
    deleteJobTitle(id: $id) {
      id
    }
  }
`;

export function useDeleteJobTitleMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    DeleteJobTitleMutation,
    DeleteJobTitleMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    DeleteJobTitleMutation,
    DeleteJobTitleMutationVariables
  >(DeleteJobTitleDocument, baseOptions);
}
export type DeleteJobTitleMutationHookResult = ReturnType<
  typeof useDeleteJobTitleMutation
>;
export const ListLocationsDocument = gql`
  query ListLocations {
    listLocations {
      id
      locationName
    }
  }
`;

export function useListLocationsQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ListLocationsQueryVariables>
) {
  return ReactApolloHooks.useQuery<
    ListLocationsQuery,
    ListLocationsQueryVariables
  >(ListLocationsDocument, baseOptions);
}
export type ListLocationsQueryHookResult = ReturnType<
  typeof useListLocationsQuery
>;
export const ViewLocationDocument = gql`
  query ViewLocation($id: String!) {
    viewLocation(id: $id) {
      id
      locationName
    }
  }
`;

export function useViewLocationQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ViewLocationQueryVariables>
) {
  return ReactApolloHooks.useQuery<
    ViewLocationQuery,
    ViewLocationQueryVariables
  >(ViewLocationDocument, baseOptions);
}
export type ViewLocationQueryHookResult = ReturnType<
  typeof useViewLocationQuery
>;
export const CreateLocationDocument = gql`
  mutation CreateLocation($locationName: String!) {
    createLocation(locationName: $locationName) {
      id
      locationName
    }
  }
`;

export function useCreateLocationMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateLocationMutation,
    CreateLocationMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateLocationMutation,
    CreateLocationMutationVariables
  >(CreateLocationDocument, baseOptions);
}
export type CreateLocationMutationHookResult = ReturnType<
  typeof useCreateLocationMutation
>;
export const UpdateLocationDocument = gql`
  mutation UpdateLocation($id: String!, $locationName: String!) {
    updateLocation(id: $id, locationName: $locationName) {
      id
      locationName
    }
  }
`;

export function useUpdateLocationMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateLocationMutation,
    UpdateLocationMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateLocationMutation,
    UpdateLocationMutationVariables
  >(UpdateLocationDocument, baseOptions);
}
export type UpdateLocationMutationHookResult = ReturnType<
  typeof useUpdateLocationMutation
>;
export const DeleteLocationDocument = gql`
  mutation DeleteLocation($id: String!) {
    deleteLocation(id: $id) {
      id
    }
  }
`;

export function useDeleteLocationMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    DeleteLocationMutation,
    DeleteLocationMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    DeleteLocationMutation,
    DeleteLocationMutationVariables
  >(DeleteLocationDocument, baseOptions);
}
export type DeleteLocationMutationHookResult = ReturnType<
  typeof useDeleteLocationMutation
>;
export const UserLookupDocument = gql`
  query UserLookup {
    userLookup {
      JobTitles {
        id
        jobTitleName
      }
      Departments {
        id
        departmentName
      }
      Locations {
        id
        locationName
      }
    }
  }
`;

export function useUserLookupQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<UserLookupQueryVariables>
) {
  return ReactApolloHooks.useQuery<UserLookupQuery, UserLookupQueryVariables>(
    UserLookupDocument,
    baseOptions
  );
}
export type UserLookupQueryHookResult = ReturnType<typeof useUserLookupQuery>;
export const ProjectLookupDocument = gql`
  query ProjectLookup {
    projectLookup {
      Users {
        id
        firstName
        lastName
        emailAddress
        profilePhoto
      }
    }
  }
`;

export function useProjectLookupQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ProjectLookupQueryVariables>
) {
  return ReactApolloHooks.useQuery<
    ProjectLookupQuery,
    ProjectLookupQueryVariables
  >(ProjectLookupDocument, baseOptions);
}
export type ProjectLookupQueryHookResult = ReturnType<
  typeof useProjectLookupQuery
>;
export const StatusTypesDocument = gql`
  query StatusTypes {
    statusTypes {
      id
      text
    }
  }
`;

export function useStatusTypesQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<StatusTypesQueryVariables>
) {
  return ReactApolloHooks.useQuery<StatusTypesQuery, StatusTypesQueryVariables>(
    StatusTypesDocument,
    baseOptions
  );
}
export type StatusTypesQueryHookResult = ReturnType<typeof useStatusTypesQuery>;
export const ListProjectsDocument = gql`
  query ListProjects {
    listProjects {
      id
      projectName
      description
      dateCompletion
      ownerUserId
      projectStatusId
      projectModuleList {
        id
        moduleName
      }
      projectNoteList {
        id
        noteName
      }
      projectUserList {
        id
        userId
        accepted
      }
    }
  }
`;

export function useListProjectsQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ListProjectsQueryVariables>
) {
  return ReactApolloHooks.useQuery<
    ListProjectsQuery,
    ListProjectsQueryVariables
  >(ListProjectsDocument, baseOptions);
}
export type ListProjectsQueryHookResult = ReturnType<
  typeof useListProjectsQuery
>;
export const CreateProjectDocument = gql`
  mutation CreateProject(
    $projectName: String!
    $ownerUserId: String!
    $description: String!
  ) {
    createProject(
      projectName: $projectName
      ownerUserId: $ownerUserId
      description: $description
    ) {
      id
      ownerUserId
      projectName
      description
      dateCompletion
    }
  }
`;

export function useCreateProjectMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateProjectMutation,
    CreateProjectMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateProjectMutation,
    CreateProjectMutationVariables
  >(CreateProjectDocument, baseOptions);
}
export type CreateProjectMutationHookResult = ReturnType<
  typeof useCreateProjectMutation
>;
export const DeleteProjectDocument = gql`
  mutation DeleteProject($id: String!) {
    deleteProject(id: $id) {
      id
    }
  }
`;

export function useDeleteProjectMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    DeleteProjectMutation,
    DeleteProjectMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    DeleteProjectMutation,
    DeleteProjectMutationVariables
  >(DeleteProjectDocument, baseOptions);
}
export type DeleteProjectMutationHookResult = ReturnType<
  typeof useDeleteProjectMutation
>;
export const ListTasksDocument = gql`
  query ListTasks {
    listProjectTask {
      id
      taskName
      ownerUserId
      dateStart
      dateEnd
      dateTarget
      statusType
      priority
      projectType {
        id
        projectName
      }
    }
  }
`;

export function useListTasksQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ListTasksQueryVariables>
) {
  return ReactApolloHooks.useQuery<ListTasksQuery, ListTasksQueryVariables>(
    ListTasksDocument,
    baseOptions
  );
}
export type ListTasksQueryHookResult = ReturnType<typeof useListTasksQuery>;
export const ListProjectTaskByProjectDocument = gql`
  query ListProjectTaskByProject($projectType: String!) {
    listProjectTaskByProject(projectType: $projectType) {
      id
      taskName
      dateStart
      dateEnd
      dateTarget
      statusType
      priority
      projectType {
        id
      }
    }
  }
`;

export function useListProjectTaskByProjectQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<
    ListProjectTaskByProjectQueryVariables
  >
) {
  return ReactApolloHooks.useQuery<
    ListProjectTaskByProjectQuery,
    ListProjectTaskByProjectQueryVariables
  >(ListProjectTaskByProjectDocument, baseOptions);
}
export type ListProjectTaskByProjectQueryHookResult = ReturnType<
  typeof useListProjectTaskByProjectQuery
>;
export const ViewProjectTaskDocument = gql`
  query ViewProjectTask($taskId: String!, $projectType: String!) {
    viewProjectTask(id: $taskId, projectType: $projectType) {
      id
      taskName
      ownerUserId
      dateStart
      dateEnd
      dateTarget
      statusType
      testUserId
      priority
      projectType {
        id
      }
    }
  }
`;

export function useViewProjectTaskQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ViewProjectTaskQueryVariables>
) {
  return ReactApolloHooks.useQuery<
    ViewProjectTaskQuery,
    ViewProjectTaskQueryVariables
  >(ViewProjectTaskDocument, baseOptions);
}
export type ViewProjectTaskQueryHookResult = ReturnType<
  typeof useViewProjectTaskQuery
>;
export const CreateProjectTaskDocument = gql`
  mutation CreateProjectTask(
    $taskName: String!
    $ownerUserId: String!
    $projectType: String!
    $dateStart: String
    $dateEnd: String
    $statusType: String
  ) {
    createProjectTask(
      taskName: $taskName
      ownerUserId: $ownerUserId
      projectType: $projectType
      dateStart: $dateStart
      dateEnd: $dateEnd
      statusType: $statusType
    ) {
      taskName
    }
  }
`;

export function useCreateProjectTaskMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateProjectTaskMutation,
    CreateProjectTaskMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateProjectTaskMutation,
    CreateProjectTaskMutationVariables
  >(CreateProjectTaskDocument, baseOptions);
}
export type CreateProjectTaskMutationHookResult = ReturnType<
  typeof useCreateProjectTaskMutation
>;
export const DeleteProjectTaskDocument = gql`
  mutation DeleteProjectTask($id: String!, $projectType: String!) {
    deleteProjectTask(id: $id, projectType: $projectType) {
      id
    }
  }
`;

export function useDeleteProjectTaskMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    DeleteProjectTaskMutation,
    DeleteProjectTaskMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    DeleteProjectTaskMutation,
    DeleteProjectTaskMutationVariables
  >(DeleteProjectTaskDocument, baseOptions);
}
export type DeleteProjectTaskMutationHookResult = ReturnType<
  typeof useDeleteProjectTaskMutation
>;
export const UpdateDateStartDocument = gql`
  mutation UpdateDateStart(
    $taskId: String!
    $projectType: String!
    $dateStart: String!
  ) {
    updateProjectTask(
      id: $taskId
      projectType: $projectType
      dateStart: $dateStart
    ) {
      id
      dateStart
    }
  }
`;

export function useUpdateDateStartMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateDateStartMutation,
    UpdateDateStartMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateDateStartMutation,
    UpdateDateStartMutationVariables
  >(UpdateDateStartDocument, baseOptions);
}
export type UpdateDateStartMutationHookResult = ReturnType<
  typeof useUpdateDateStartMutation
>;
export const UpdateProjectTaskFieldDocument = gql`
  mutation UpdateProjectTaskField(
    $taskId: String!
    $projectType: String!
    $field: String!
    $value: String
  ) {
    updateProjectTaskField(
      id: $taskId
      projectType: $projectType
      field: $field
      value: $value
    ) {
      id
    }
  }
`;

export function useUpdateProjectTaskFieldMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateProjectTaskFieldMutation,
    UpdateProjectTaskFieldMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateProjectTaskFieldMutation,
    UpdateProjectTaskFieldMutationVariables
  >(UpdateProjectTaskFieldDocument, baseOptions);
}
export type UpdateProjectTaskFieldMutationHookResult = ReturnType<
  typeof useUpdateProjectTaskFieldMutation
>;
export const PostCommentDocument = gql`
  mutation PostComment($taskType: String!, $postMessage: String!) {
    postComment(taskType: $taskType, postMessage: $postMessage) {
      id
      userType
      datePosted
      postMessage
    }
  }
`;

export function usePostCommentMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    PostCommentMutation,
    PostCommentMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    PostCommentMutation,
    PostCommentMutationVariables
  >(PostCommentDocument, baseOptions);
}
export type PostCommentMutationHookResult = ReturnType<
  typeof usePostCommentMutation
>;
export const GetTaskCommentsByTaskDocument = gql`
  query GetTaskCommentsByTask($taskType: String!) {
    getTaskCommentsByTask(taskType: $taskType) {
      id
      userType
      postMessage
      datePosted
    }
  }
`;

export function useGetTaskCommentsByTaskQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<
    GetTaskCommentsByTaskQueryVariables
  >
) {
  return ReactApolloHooks.useQuery<
    GetTaskCommentsByTaskQuery,
    GetTaskCommentsByTaskQueryVariables
  >(GetTaskCommentsByTaskDocument, baseOptions);
}
export type GetTaskCommentsByTaskQueryHookResult = ReturnType<
  typeof useGetTaskCommentsByTaskQuery
>;
export const ListTaskTodoByTaskDocument = gql`
  query ListTaskTodoByTask($taskType: String!) {
    listTaskTodoByTask(taskType: $taskType) {
      id
      todo
      dateCreated
      isCompleted
      taskType
    }
  }
`;

export function useListTaskTodoByTaskQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<
    ListTaskTodoByTaskQueryVariables
  >
) {
  return ReactApolloHooks.useQuery<
    ListTaskTodoByTaskQuery,
    ListTaskTodoByTaskQueryVariables
  >(ListTaskTodoByTaskDocument, baseOptions);
}
export type ListTaskTodoByTaskQueryHookResult = ReturnType<
  typeof useListTaskTodoByTaskQuery
>;
export const UpdateTaskTodoStringFieldDocument = gql`
  mutation UpdateTaskTodoStringField(
    $taskId: String!
    $id: String!
    $field: String!
    $value: String!
  ) {
    updateTaskTodoStringField(
      id: $id
      taskType: $taskId
      field: $field
      value: $value
    ) {
      id
    }
  }
`;

export function useUpdateTaskTodoStringFieldMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateTaskTodoStringFieldMutation,
    UpdateTaskTodoStringFieldMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateTaskTodoStringFieldMutation,
    UpdateTaskTodoStringFieldMutationVariables
  >(UpdateTaskTodoStringFieldDocument, baseOptions);
}
export type UpdateTaskTodoStringFieldMutationHookResult = ReturnType<
  typeof useUpdateTaskTodoStringFieldMutation
>;
export const UpdateTaskTodoBooleanFieldDocument = gql`
  mutation UpdateTaskTodoBooleanField(
    $taskId: String!
    $id: String!
    $field: String!
    $value: Boolean!
  ) {
    updateTaskTodoBooleanField(
      id: $id
      taskType: $taskId
      field: $field
      value: $value
    ) {
      id
    }
  }
`;

export function useUpdateTaskTodoBooleanFieldMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateTaskTodoBooleanFieldMutation,
    UpdateTaskTodoBooleanFieldMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateTaskTodoBooleanFieldMutation,
    UpdateTaskTodoBooleanFieldMutationVariables
  >(UpdateTaskTodoBooleanFieldDocument, baseOptions);
}
export type UpdateTaskTodoBooleanFieldMutationHookResult = ReturnType<
  typeof useUpdateTaskTodoBooleanFieldMutation
>;
export const CreateTaskTodoDocument = gql`
  mutation CreateTaskTodo($taskType: String!, $todo: String!) {
    createTaskTodo(todo: $todo, taskType: $taskType) {
      id
    }
  }
`;

export function useCreateTaskTodoMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateTaskTodoMutation,
    CreateTaskTodoMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateTaskTodoMutation,
    CreateTaskTodoMutationVariables
  >(CreateTaskTodoDocument, baseOptions);
}
export type CreateTaskTodoMutationHookResult = ReturnType<
  typeof useCreateTaskTodoMutation
>;
export const DeleteTaskTodoDocument = gql`
  mutation DeleteTaskTodo($taskType: String!, $id: String!) {
    deleteTaskTodo(id: $id, taskType: $taskType) {
      id
    }
  }
`;

export function useDeleteTaskTodoMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    DeleteTaskTodoMutation,
    DeleteTaskTodoMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    DeleteTaskTodoMutation,
    DeleteTaskTodoMutationVariables
  >(DeleteTaskTodoDocument, baseOptions);
}
export type DeleteTaskTodoMutationHookResult = ReturnType<
  typeof useDeleteTaskTodoMutation
>;
export const ListUsersDocument = gql`
  query ListUsers {
    listUsers {
      id
      firstName
      lastName
      emailAddress
      workId
    }
  }
`;

export function useListUsersQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ListUsersQueryVariables>
) {
  return ReactApolloHooks.useQuery<ListUsersQuery, ListUsersQueryVariables>(
    ListUsersDocument,
    baseOptions
  );
}
export type ListUsersQueryHookResult = ReturnType<typeof useListUsersQuery>;
export const ViewUserDocument = gql`
  query ViewUser($id: String!) {
    viewUser(id: $id) {
      id
      firstName
      lastName
      emailAddress
      dateOfBirth
      gender
      profilePhoto
      homePhoneNumber
      cellPhoneNumber
      address
      city
      suburb
      countryState
      postcode
      tfn
      bankName
      branch
      accountName
      bsb
      accountNumber
      emergencyContactName
      emergencyContactNumber
      emergencyContactAddress
      emergencyContactCity
      emergencyContactSuburb
      emergencyContactState
      emergencyContactPostcode
      emergencyContactRelationship
      workId
      workStartDate
      workTitleType {
        id
        jobTitleName
      }
      workPhoneNumber
      workCellphoneNumber
      workDepartmentType {
        id
        departmentName
      }
      workLocationType {
        id
        locationName
      }
      workEmailAddress
    }
  }
`;

export function useViewUserQuery(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ViewUserQueryVariables>
) {
  return ReactApolloHooks.useQuery<ViewUserQuery, ViewUserQueryVariables>(
    ViewUserDocument,
    baseOptions
  );
}
export type ViewUserQueryHookResult = ReturnType<typeof useViewUserQuery>;
export const CreateUserDocument = gql`
  mutation CreateUser($id: String!, $firstName: String!, $lastName: String!) {
    createUser(id: $id, firstName: $firstName, lastName: $lastName) {
      id
      firstName
      lastName
    }
  }
`;

export function useCreateUserMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateUserMutation,
    CreateUserMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateUserMutation,
    CreateUserMutationVariables
  >(CreateUserDocument, baseOptions);
}
export type CreateUserMutationHookResult = ReturnType<
  typeof useCreateUserMutation
>;
export const DeleteUserDocument = gql`
  mutation DeleteUser($id: String!) {
    deleteUser(id: $id) {
      id
    }
  }
`;

export function useDeleteUserMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    DeleteUserMutation,
    DeleteUserMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    DeleteUserMutation,
    DeleteUserMutationVariables
  >(DeleteUserDocument, baseOptions);
}
export type DeleteUserMutationHookResult = ReturnType<
  typeof useDeleteUserMutation
>;
export const UpdateUserDocument = gql`
  mutation UpdateUser(
    $id: String!
    $firstName: String!
    $lastName: String!
    $emailAddress: String
    $dateOfBirth: String
    $gender: GenderTEnum
    $profilePhoto: String
    $homePhoneNumber: String
    $cellPhoneNumber: String
    $address: String
    $city: String
    $suburb: String
    $countryState: String
    $postcode: String
    $tfn: String
    $bankName: String
    $branch: String
    $accountName: String
    $bsb: String
    $accountNumber: String
    $emergencyContactName: String
    $emergencyContactNumber: String
    $emergencyContactAddress: String
    $emergencyContactCity: String
    $emergencyContactSuburb: String
    $emergencyContactState: String
    $emergencyContactPostcode: String
    $emergencyContactRelationship: String
    $workId: String
    $workStartDate: String
    $workTitleType: String
    $workPhoneNumber: String
    $workCellphoneNumber: String
    $workDepartmentType: String
    $workLocationType: String
    $workEmailAddress: String
  ) {
    updateUser(
      id: $id
      firstName: $firstName
      lastName: $lastName
      emailAddress: $emailAddress
      dateOfBirth: $dateOfBirth
      gender: $gender
      profilePhoto: $profilePhoto
      homePhoneNumber: $homePhoneNumber
      cellPhoneNumber: $cellPhoneNumber
      address: $address
      city: $city
      suburb: $suburb
      countryState: $countryState
      postcode: $postcode
      tfn: $tfn
      bankName: $bankName
      branch: $branch
      accountName: $accountName
      bsb: $bsb
      accountNumber: $accountNumber
      emergencyContactName: $emergencyContactName
      emergencyContactNumber: $emergencyContactNumber
      emergencyContactAddress: $emergencyContactAddress
      emergencyContactCity: $emergencyContactCity
      emergencyContactSuburb: $emergencyContactSuburb
      emergencyContactState: $emergencyContactState
      emergencyContactPostcode: $emergencyContactPostcode
      emergencyContactRelationship: $emergencyContactRelationship
      workId: $workId
      workStartDate: $workStartDate
      workTitleType: $workTitleType
      workPhoneNumber: $workPhoneNumber
      workCellphoneNumber: $workCellphoneNumber
      workDepartmentType: $workDepartmentType
      workLocationType: $workLocationType
      workEmailAddress: $workEmailAddress
    ) {
      id
      firstName
      lastName
      emailAddress
      dateOfBirth
      gender
      profilePhoto
      homePhoneNumber
      cellPhoneNumber
      address
      city
      suburb
      countryState
      postcode
      tfn
      bankName
      branch
      accountName
      bsb
      accountNumber
      emergencyContactName
      emergencyContactNumber
      emergencyContactAddress
      emergencyContactCity
      emergencyContactSuburb
      emergencyContactState
      emergencyContactPostcode
      emergencyContactRelationship
      workId
      workStartDate
      workTitleType {
        id
        jobTitleName
      }
      workPhoneNumber
      workCellphoneNumber
      workDepartmentType {
        id
        departmentName
      }
      workLocationType {
        id
        locationName
      }
      workEmailAddress
    }
  }
`;

export function useUpdateUserMutation(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    UpdateUserMutation,
    UpdateUserMutationVariables
  >
) {
  return ReactApolloHooks.useMutation<
    UpdateUserMutation,
    UpdateUserMutationVariables
  >(UpdateUserDocument, baseOptions);
}
export type UpdateUserMutationHookResult = ReturnType<
  typeof useUpdateUserMutation
>;
