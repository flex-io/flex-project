import mocker from "mocker-data-generator";
import moment from "moment";

export function generateObj(headers: any) {
	const obj = {};

	headers.forEach(
		(header: {
			key: string;
			dataIndex: string | number | symbol;
			type: string;
		}) => {
			let objValue = {};
			if (header.key === "id") {
				objValue = { chance: "guid" };
			} else if (header.key === "email") {
				objValue = { faker: "internet.email" };
			} else if (header.type === "percentage") {
				objValue = { faker: 'random.number({"min": 0, "max": 100})' };
			} else if (header.type === "name") {
				objValue = { faker: 'name.findName' };
			} else if (header.key.includes("date")) {
				objValue = {
					function: () => {
						return moment(
							new Date(
								+new Date() -
									Math.floor(Math.random() * 10000000000)
							)
						).format("MM/DD/YYYY");
					}
				};
			} else objValue = { faker: "lorem.words()" };

			Object.defineProperty(obj, header.dataIndex, {
				value: objValue,
				writable: true,
				enumerable: true,
				configurable: true
			});
		}
	);

	return obj;
}

export class MockData {
	static getList(headers: any, count: number) {
		const obj = generateObj(headers);

		return [
			mocker()
				.schema("project", obj, count)
				.build((_error, data) => {
					return data.project;
				})
		];
	}

	static getOne(headers: any) {
		return mocker()
			.schema("project", headers, 1)
			.build((_error, data) => {
				return data.project[0];
			});
	}
}
