import config from "../../src/config";

export const shortid = require("shortid");

// dont use other characters except these. you might receive an error in css id or className
shortid.characters("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-");

export const moment = require("moment");
moment.locale("en-au");

export const getAccessToken = () => {
	const cognitoServiceProvider = `CognitoIdentityServiceProvider.${config.cognito.APP_CLIENT_ID}`;
	const lastAuthUser = localStorage.getItem(`${cognitoServiceProvider}.LastAuthUser`);

	return localStorage.getItem(`${cognitoServiceProvider}.${lastAuthUser}.accessToken`);
};
