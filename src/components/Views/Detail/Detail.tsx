import React, { PureComponent } from "react";
import glamorous from "glamorous";
import Section from "./Section";
import { Button } from "components/uikit";
import { shortid } from "helper";

interface IAction {
	text: string;
	key: string;
	callback: () => void;
}

export interface DetailProps {
	title: string;
	onTitleUpdate?(callback: string): void;
	actions: IAction[];
}

const TitleInput = glamorous.input({
	border: "none",
	// padding: "4px 8px",
	width: "100%",
	cursor: "default",
	lineHeight: "1.5rem",
	fontSize: "1rem",
	backgroundColor: "transparent",
	":focus": {
		outline: "none",
		border: "1px solid black",
		cursor: "text"
	}
});

export class Detail extends PureComponent<DetailProps> {
	static Section = Section;
	render() {
		let headerInput: any;
		const { title, actions } = this.props;
		return (
			<Details>
				<DetailsBody>
					{actions.map(action => {
						return (
							<FlexRTL key={shortid.generate()}>
								<Button
									button_type="link"
									onClick={e => {
										e.preventDefault();
										action.callback();
									}}
								>
									{action.text}
								</Button>
							</FlexRTL>
						);
					})}
					<DetailHeader>
						<TitleInput
							innerRef={ref => (headerInput = ref)}
							defaultValue={title}
							onKeyDown={e => {
								if (e.key === "Enter") {
									this.props.onTitleUpdate(headerInput.value);
									headerInput.blur();
								} else if (e.key === "Escape") headerInput.blur();
							}}
						/>
					</DetailHeader>

					{this.props.children}
				</DetailsBody>
			</Details>
		);
	}
}

const FlexRTL = glamorous.div({
	display: "flex",
	flexDirection: "row-reverse"
});

const Details = glamorous.div({
	width: 450,
	backgroundColor: "#F3F2F1",
	borderLeft: "1px solid #eaeaea",
	position: "relative"
});

const DetailsBody = glamorous.div({
	overflow: "auto",
	marginTop: 10,
	padding: "0 10px 16px 10px",
	overflowX: "hidden",
	height: "100%"
});

const DetailHeader = glamorous.div({
	zIndex: 10,
	position: "sticky",
	top: 0,
	background: "white",
	transition: "0.3s",
	padding: 10,
	display: "flex",
	boxShadow: "0 1px 1px 0 rgba(0,0,0,0.2)"
});
