import React, { PureComponent } from "react";
import glamorous from "glamorous";
import DatePicker, { DatePickerProps } from "./Components/DatePicker";
import { Checkbox, CheckboxProps } from "./Components/Checkbox";
import Select, { SelectProps } from "./Components/Select";
import LinkList, { LinkListProps } from "./Components/LinkList";
// interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
// 	text: string;
// 	icon?: string;
// }

// Button, Checkbox, Option, List, Input, Tabs
interface SectionComponentProps {
	Datepicker: React.ComponentType<DatePickerProps>;
	Checkbox: React.ComponentType<CheckboxProps>;
	Select: React.ComponentType<SelectProps>;
	LinkList: React.ComponentType<LinkListProps>;
}

interface SectionProps {
	children(props: SectionComponentProps): JSX.Element;
	padding?: number;
	Header?: string | React.ComponentType;
}

// interface DefaultProps {
// 	text?: string;
// 	icon?: string;
// }

const defaultProps: Partial<SectionProps> = {
	padding: 0
};

export default class Section extends PureComponent<SectionProps> {
	render() {
		const { Header } = this.props;
		return (
			<>
				{Header && <>{typeof Header === "function" ? <Header /> : <div>{Header}</div>}</>}
				<SectionContainer padding={this.props.padding}>
					{this.props.children({
						Datepicker: DatePicker,
						Checkbox: Checkbox,
						Select: Select,
						LinkList: LinkList
					})}
				</SectionContainer>
			</>
		);
	}
}

export const SectionInner = glamorous.div({
	display: "flex",
	flex: "auto",
	padding: "4px 0"
});

export const SectionContent = glamorous.div({
	marginRight: "8px",
	marginLeft: "8px",
	overflow: "hidden",
	display: "flex"
});

export const SectionTitle = glamorous.div({
	fontSize: "0.9rem",
	lineHeight: "2.3rem",
	color: "#767678"
});

export const SectionIcon = glamorous.div({
	margin: "0 4px",
	display: "flex",
	alignItems: "center",
	justifyContent: "center",
	width: "24px",
	textAlign: "center",
	color: "#767678"
});

export const SectionItem = glamorous.div<{ isLast?: boolean; className?: string }>(
	{
		minHeight: 52,
		// position: "relative",
		display: "flex",

		":hover button": {
			opacity: 1
		}
	},
	props => (
		props.className,
		props.isLast !== true && {
			boxShadow: "24px 25px 0 -24px rgba(0, 0, 0, 0.08)"
		}
	)
);

const SectionContainer = glamorous.div<{ padding: number }>(
	{
		// zIndex={5}
		// position="relative"
		backgroundColor: "#FFFFFF",
		border: "1px solid rgba(0 0 0 0.08)",
		borderRadius: 2,
		// display="flex"
		// flexDirection="column"
		margin: "10px 0 10px 0"
		// height="100%"
	},
	props => ({
		padding: props.padding
	})
);

export const ButtonSectionDelete = glamorous.button({
	opacity: 0,
	background: "none",
	boxShadow: "none",
	border: "none",
	outline: "none",
	cursor: "pointer",
	height: 32,
	width: 32,
	// padding: "6px 10px",
	margin: "0 2px",
	color: "#767678",
	// textAlign: "center",
	justifyContent: "center",
	// alignItems: "center",
	display: "flex",
	alignSelf: "center"
});

SectionContainer.defaultProps = defaultProps;
