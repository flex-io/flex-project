import React, { FC, useRef } from "react";
import glamorous from "glamorous";
import { ButtonSectionDelete, SectionItem } from "../Section";

const SelectGl = glamorous.select({
	width: "100%",
	display: "block",
	background: "none",
	fontSize: "16px",
	fontFamily: "sans-serif",
	// fontWeight: 700,
	color: "#444",
	lineHeight: "1",
	padding: ".6em 1.4em .5em .8em",
	maxWidth: "100%",
	outline: "none",
	// boxSizing: "border-box",
	margin: "4",
	border: "none",
	// boxShadow: "0 1px 0 1px rgba(0,0,0,.04)",
	// borderRadius: ".5em",
	appearance: "none",
	backgroundColor: "#fff",
	backgroundRepeat: "no-repeat, repeat"
	// backgroundPosition: "right .7em top 50%, 0 0",
	// backgroundSize: ".65em auto, 100%"
});

const Option = glamorous.option({
	fontWeight: "normal"
});

interface IOption {
	id: string;
	text: string;
}

export interface SelectProps extends React.SelectHTMLAttributes<HTMLSelectElement> {
	options: IOption[];
	defaultText?: string;
	onChanged?(opt: any): void;
}

const defaultProps: Partial<SelectProps> = {
	defaultText: "Select"
};

const Select: FC<SelectProps> = props => {
	const element = useRef(null);

	return (
		<SectionItem>
			<SelectGl
				{...props}
				innerRef={element}
				defaultValue={
					props.defaultValue === null ? "default" : props.defaultValue
				}
				onChange={(e: any) => {
					props.onChanged && props.onChanged(e.target.value);
				}}
			>
				<Option value={"default"} disabled>
					{props.defaultText}
				</Option>
				{props.options.map(option => {
					return (
						<Option value={option.id} key={option.id}>
							{option.text}
						</Option>
					);
				})}
			</SelectGl>
			<ButtonSectionDelete
				onClick={(e: any) => {
					e.preventDefault();
					element.current.value = "default";
					props.onChanged && props.onChanged(null);
				}}
			>
				{"X"}
			</ButtonSectionDelete>
		</SectionItem>
	);
};

Select.defaultProps = defaultProps;

export default Select;
