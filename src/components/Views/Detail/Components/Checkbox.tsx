import React, { FC } from "react";
import glamorous from "glamorous";

export interface CheckboxProps extends React.InputHTMLAttributes<HTMLInputElement> {
	text: string;
	onDelete?(): void;
	onChanged?(value: boolean): void;
	strikeThrough?: boolean;
}

const defaultProps: Partial<CheckboxProps> = {
	strikeThrough: false
};

const BlockContainer = glamorous.div<{ boxShadow?: number }>(
	{
		display: "flex",
		padding: "12px 0 12px 16px",
		":hover a": {
			opacity: 1
		},
		"> *": {
			alignSelf: "center"
		}
	},
	props => ({
		height: props.boxShadow + 1,
		boxShadow: `${props.boxShadow}px ${props.boxShadow + 1}px 0 ${props.boxShadow * -1}px rgba(0, 0, 0, 0.08)`
	})
);

const CheckboxLabel = glamorous.label<{ isStrikeThrough?: boolean }>(
	{
		cursor: "pointer",
		width: "100%"
	},
	props => ({
		textDecoration: props.isStrikeThrough ? "line-through" : "initial"
	})
);

const CheckboxInput = glamorous.input({
	marginRight: 10
});

const ButtonSectionDelete = glamorous.button({
	opacity: 0,
	background: "none",
	boxShadow: "none",
	border: "none",
	outline: "none",
	cursor: "pointer",
	// height: 32,
	// width: 32,
	// padding: "6px 10px",
	margin: "0 2px",
	color: "#767678",
	// textAlign: "center",
	justifyContent: "center",
	// alignItems: "center",
	display: "flex",
	alignSelf: "center"
});

export const Checkbox: FC<CheckboxProps> = props => {
	let checkbox: any;
	return (
		<BlockContainer boxShadow={24}>
			<CheckboxLabel isStrikeThrough={props.strikeThrough}>
				<CheckboxInput
					innerRef={ref => (checkbox = ref)}
					className="uk-checkbox"
					type="checkbox"
					{...props}
					onChange={() => {
						props.onChanged && props.onChanged(checkbox.checked);
					}}
				/>{" "}
				{props.text}
			</CheckboxLabel>
			{props.onDelete && <ButtonSectionDelete onClick={props.onDelete}>{"X"}</ButtonSectionDelete>}
		</BlockContainer>
	);
};

Checkbox.defaultProps = defaultProps;
