import React, { FC } from "react";
import glamorous from "glamorous";
import { shortid } from "helper";

interface LinkItemProps {
	text: string;
	to?: string;
}

const LinkItemComponent = glamorous.a({});

const LinkItem: FC<LinkItemProps> = props => {
	return (
		<LinkContainer>
			<LinkItemComponent href={props.to}>{props.text}</LinkItemComponent>
		</LinkContainer>
	);
};

const LinkContainer = glamorous.div({});

export interface LinkListProps {
	items?: LinkItemProps[];
}

const defaultProps: Partial<LinkListProps> = {
	items: [
		{
			text: "Hello.txt",
			to: "/hellotxt"
		},
		{
			text: "World.doc",
			to: "/worlddoc"
		}
	]
};

const LinkList: FC<LinkListProps> = props => {
	console.log(props.items)
	return (
		<Container>
			{props.items.map(item => {
				return <LinkItem key={shortid.generate()} text={item.text} to={item.to} />;
			})}
		</Container>
	);
};

LinkList.defaultProps = defaultProps;

const Container = glamorous.div({
	display: "flex",
	flexDirection: "column"
});

export default LinkList;
