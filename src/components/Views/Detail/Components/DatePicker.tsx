import React, { FC, useRef, useEffect } from "react";
import glamorous from "glamorous";
import Datepicker from "react-datepicker";
import { SectionItem, SectionInner, SectionContent, SectionIcon, SectionTitle, ButtonSectionDelete } from "../Section";
import { moment } from "helper";

export interface DatePickerProps {
	text?: string;
	icon?: string;
	format?: string;
	caption?: string;
	defaultValue?: string;
	onChanged?(date: any): void;
}

const defaultProps: Partial<DatePickerProps> = {
	format: "LL"
};

const Button = glamorous.button({
	background: "none",
	width: "100%",
	// height: "100%",
	border: "none",
	boxShadow: "none",
	outline: "none",
	padding: 0,
	margin: 0
});



const DatePicker: FC<DatePickerProps> = props => {
	const node = useRef();
	const [isOpen, openPopper] = React.useState(false);
	const [date, setDate] = React.useState(props.defaultValue);

	function handleChange(date: any) {
		setDate(date);
		if (date) openPopper(!isOpen);
		props.onChanged && props.onChanged(date);
	}
	useEffect(() => {
		document.addEventListener("mousedown", handleClick);

		return () => {
			document.removeEventListener("mousedown", handleClick);
		};
	}, [isOpen]);
	const handleClick = (e: any) => {
		//@ts-ignore
		if (node.current.contains(e.target)) {
			// inside click
			return;
		}

		openPopper(false);
		// outside click
		// ... do whatever on click outside here ...
	};
	return (
		<SectionItem innerRef={node}>
			<Button onClick={() => openPopper(!isOpen)} type="button">
				<SectionInner>
					<SectionContent>
						<SectionIcon>
							<div uk-icon={props.icon} />
						</SectionIcon>
					</SectionContent>
					<SectionTitle>
						{`${props.caption}`}
						{date && moment(date).format(props.format)}
					</SectionTitle>
				</SectionInner>
			</Button>
			{date && (
				<ButtonSectionDelete
					onClick={(e: any) => {
						e.preventDefault();
						handleChange(null);
					}}
				>
					{"X"}
				</ButtonSectionDelete>
			)}
			{isOpen && (
				<Container>
					<Datepicker selected={new Date()} inline onChange={handleChange} dateFormat="MM/dd/yyyy" />
				</Container>
			)}
		</SectionItem>
	);
};

DatePicker.defaultProps = defaultProps;

const Container = glamorous.div({
	position: "absolute",
	zIndex: 1000,
	boxSizing: "border-box",
	marginTop: 50
});

export default DatePicker;
