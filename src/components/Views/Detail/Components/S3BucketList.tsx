import React, { FC, useState, useEffect } from "react";
import { Storage } from "aws-amplify";
import glamorous from "glamorous";
import { shortid } from "helper";

interface S3BucketListProps {
	path: string;
	level: string;
}

const Container = glamorous.div({
	display: "flex",
	flexDirection: "column"
});

const S3BucketList: FC<S3BucketListProps> = props => {
	const [files, setfiles] = useState([]);
	useEffect(() => {
		Storage.list(props.path, { level: props.level }).then(res => {
			setfiles(res);
		});
	}, []);
	console.log(files);
	return (
		<Container>
			{files &&
				files.map((file: any) => {
					return (
						<a
							key={shortid.generate()}
							target="_blank"
							href={`https://flex-notes-app-uploads.s3-ap-southeast-2.amazonaws.com/${props.level}/${file.key}`}
						>
							{file.key.replace(`${props.path}/`, "")}
						</a>
					);
				})}
		</Container>
	);
};

export default S3BucketList;
