import React, { FC, useRef, useEffect } from "react";
import glamorous from "glamorous";
import { Button } from "components/uikit";
import { shortid } from "helper";

export interface CommentListProps {
	callbackMessage?(value: any): void;
	comments: CommentProps[];
}

interface CommentProps {}

const Comment: FC<CommentProps> = props => {
	return (
		<CommentContainer>
			<span uk-icon="icon: user"></span>
			<MessageContainer>
				<Commentator>{props.userId}</Commentator>
				<CommentText>{props.text}</CommentText>
			</MessageContainer>
		</CommentContainer>
	);
};

const MessageContainer = glamorous.div({
	display: "flex",
	flexDirection: "column",
	flex: 1,
	backgroundColor: "#FFF"
});
const Commentator = glamorous.div({});
const CommentText = glamorous.div({});
const CommentContainer = glamorous.div({
	display: "flex",
	flexDirection: "row",
	marginBottom: 8
});

interface CommentProps {
	userId: string;
	text: string;
}

const CommentList: FC<CommentListProps> = props => {
	const textArea = useRef(null);
	const scroll = useRef(null);

	useEffect(() => {
		scroll.current.scrollTop = 1000;
	}, []);
	return (
		<Container className="app-content-stretch">
			<CommentListContainer className="scroll-container" innerRef={scroll}>
				{props.comments.map(obj => {
					return <Comment key={shortid.generate()} userId={obj.userId} text={obj.text} />;
				})}
			</CommentListContainer>
			<TypingContainer>
				<TextArea rows={4} innerRef={textArea} />
				<Button
					button_type="link"
					onClick={e => {
						e.preventDefault();

						props.callbackMessage(textArea.current.value);
						textArea.current.value = "";
					}}
				>
					Send
				</Button>
			</TypingContainer>
		</Container>
	);
};

const Container = glamorous.div({
	maxWidth: 450,
	backgroundColor: "#F3F2F1",
	borderLeft: "1px solid #eaeaea",
	position: "relative"
});
const CommentListContainer = glamorous.div({
	padding: 10
});

const TypingContainer = glamorous.div({
	display: "flex"
});

const TextArea = glamorous.textarea({
	flex: 1
});

export default CommentList;
