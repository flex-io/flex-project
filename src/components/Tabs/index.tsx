import React from "react";
import glamorous from "glamorous";

import Tab, { ITab } from "./Tab";

const ListTabs = glamorous.ul<{ position?: string }>(
	{
		paddingLeft: 0,
		listStyle: "none",
		margin: 0
	},
	props => {
		return (
			props.position === "left" && {
				display: "flex",
				flexDirection: "column"
			}
		);
	}
);

const TabTitleItem = glamorous.li<{ isActiveTab: boolean }>(
	{
		display: "inline-block",
		padding: 8,
		// paddingRight: 5,
		// paddingLeft: 5,
		transition: "all 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
		// padding: "16px 30px",
		cursor: "pointer",
		opacity: 0.4,
		":hover": {
			opacity: 1
		}
	},
	props => {
		return (
			props.isActiveTab && {
				transition: "all 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
				cursor: "default",
				opacity: 1
			}
		);
	}
);

// const ActiveTabBorder = glamorous.div<{ activeTabElement: any }>(
// 	{
// 		height: 4,
// 		backgroundColor: "#0088dd",
// 		position: "absolute",
// 		bottom: 0,
// 		transition: "all 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
// 		willChange: "left, width"
// 	},
// 	props => {
// 		return (
// 			props.activeTabElement && {
// 				width: props.activeTabElement.offsetWidth,
// 				left: props.activeTabElement.offsetLeft
// 			}
// 		);
// 	}
// );

const TabAnchorItem = glamorous.a({
	textTransform: "capitalize",
	color: "#000000",
	fontWeight: 600
});

const TabsContainer = glamorous.div<{ position?: string }>(
	{
		position: "relative",
		borderBottom: "1px solid #dfdfdf"
	},
	props => {
		return (
			props.position === "left" && {
				borderRight: "1px solid #dfdfdf"
			}
		);
	}
);

const ReactTabs = glamorous.div<{ position?: string }>(
	{
		position: "relative",
		// display: "flex",
		backgroundColor: "#F3F2F1"
	},
	props => {
		return (
			props.position === "left" && {
				display: "flex"
			}
		);
	}
);

interface State {
	tabs: ITab[];
	prevActiveTab: ITab;
	activeTab: any;
	tabsElements: any[];
}

interface Props {
	activeTab?: ITab;
	tabIdList?: any[];
	onClick?: (tab: ITab) => void;
	position?: string;
}

class Tabs extends React.Component<Props, State> {
	static Tab = Tab;

	constructor(props: Props) {
		super(props);
		this.state = {
			tabs: [],
			prevActiveTab: { id: "", title: "" },
			activeTab: this.props.activeTab || {},
			tabsElements: []
		};
	}

	componentDidUpdate() {
		if (this.props.tabIdList)
			this.state.tabs.forEach(tab => {
				if (!this.props.tabIdList.includes(tab.id)) {
					this.removeTab(tab.id);
				}
			});
	}

	addTab = (newTab: ITab) => {
		let isNewTabFound;

		for (let i in this.state.tabs) {
			let tab = this.state.tabs[i];

			if (tab.id === newTab.id) {
				isNewTabFound = true;
				break;
			}
		}

		if (!isNewTabFound) {
			this.setState((prevState, {}) => {
				return {
					tabs: prevState.tabs.concat(newTab)
				};
			});
		}
	};

	removeTab = (tabId: any) => {
		this.setState((prevState, {}) => {
			return {
				tabs: prevState.tabs.filter(tab => tab.id !== tabId)
			};
		});
	};

	onClick = (tab: any) => () => {
		this.setState((prevState, {}) => {
			return {
				prevActiveTab: prevState.activeTab,
				activeTab: tab
			};
		});
		if (this.props.onClick) {
			this.props.onClick(tab);
		}
	};

	render() {
		return (
			<ReactTabs position={this.props.position}>
				<TabsContainer position={this.props.position}>
					<ListTabs position={this.props.position}>
						{this.state.tabs.map((tab: any, index) => (
							<TabTitleItem
								key={index}
								onClick={this.onClick(tab)}
								id={tab.id}
								innerRef={tabElement => {
									if (!this.state.tabsElements[tab.id]) {
										this.setState((prevState, {}) => {
											const tabsElements = prevState.tabsElements;
											tabsElements[tab.id] = tabElement;

											return {
												tabsElements
											};
										});
									}
								}}
								isActiveTab={this.state.activeTab.id === tab.id}
							>
								<TabAnchorItem>{tab.title}</TabAnchorItem>
							</TabTitleItem>
						))}
					</ListTabs>
				</TabsContainer>

				{React.Children.map(this.props.children, child =>
					React.cloneElement(child as React.ReactElement<any>, {
						activeTab: this.state.activeTab,
						addTab: this.addTab
					})
				)}
			</ReactTabs>
		);
	}
}

export default Tabs;
