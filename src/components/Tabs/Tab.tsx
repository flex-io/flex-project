import React from "react";

export interface ITab {
	id: string;
	title?: string;
}

interface Props extends ITab {
	activeTab?: ITab;
	addTab?: (tab: ITab) => void;
}

class Tab extends React.Component<Props> {
	componentDidMount() {
		this.props.addTab({
			id: this.props.id,
			title: this.props.title
		});
	}

	render() {
		return this.props.activeTab.id === this.props.id && this.props.children;
	}
}

export default Tab;
