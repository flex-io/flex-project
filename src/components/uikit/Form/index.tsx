import React, { PureComponent } from "react";

export interface FormProps extends React.FormHTMLAttributes<HTMLFormElement> {}
interface FormInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
	label?: string;
}
interface FormSelectProps
	extends React.SelectHTMLAttributes<HTMLSelectElement> {
	label?: string;
	items: { text: string }[];
}

interface FormRadioProps extends React.InputHTMLAttributes<HTMLInputElement> {
	label?: string;
	items: { option: string; text: string; name: string }[];
}
export default class Form extends PureComponent<FormProps> {
	static FormInput(props: FormInputProps) {
		return (
			<div className="uk-margin">
				<label className="uk-form-label">{props.label}</label>
				<div className="uk-form-controls">
					<input className="uk-input" {...props} />
				</div>
			</div>
		);
	}

	static FormSelect(props: FormSelectProps) {
		return (
			<div className="uk-margin">
				<label className="uk-form-label">{props.label}</label>
				<div className="uk-form-controls">
					<select className="uk-select">
						{props.items.map((item, i) => {
							return (
								<option key={`select-${i}`}>{item.text}</option>
							);
						})}
					</select>
				</div>
			</div>
		);
	}

	static FormRadio(props: FormRadioProps) {
		return (
			<div className="uk-margin">
				<div className="uk-form-label">{props.label}</div>
				<div className="uk-form-controls uk-form-controls-text">
					{props.items.map((option, i) => {
						return (
							<label key={`option-${i}`}>
								<input
									className="uk-radio"
									type="radio"
									name={option.name}
								/>{" "}
								{option.text}
							</label>
						);
					})}
				</div>
			</div>
		);
	}
	static FormControls(props: any) {
		return <div className="uk-form-controls uk-flex uk-flex-left">{props.children}</div>;
	}
	render() {
		return (
			<form className="uk-padding uk-form-horizontal uk-margin">
				{this.props.children}
			</form>
		);
	}
}
