import React, { FC, useRef } from "react";
import glamorous from "glamorous";

export interface UploadProps {
	selectedFiles?(files: any): void;
}

const InputFile = glamorous.input({});

const Upload: FC<UploadProps> = props => {
	const inputRef = useRef(null);
	return (
		<div className="uk-form-custom">
			<span uk-icon="icon: cloud-upload"></span>
			<InputFile
				type="file"
				multiple
				innerRef={inputRef}
				onChange={() => props.selectedFiles && props.selectedFiles(inputRef.current.files)}
			/>
			<span className="uk-link" />
		</div>
	);
};

export default Upload;
