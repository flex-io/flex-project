import React, { FC } from "react";

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	button_type?: string;
}

const defaultProps: Partial<ButtonProps> = {
	button_type: "primary",
	onClick: (e: any) => {
		e.preventDefault();
	}
};

const Button: FC<ButtonProps> = props => {
	return (
		<button
			{...props}
			className={`uk-button uk-button-${props.button_type}`}
		>
			{props.children}
		</button>
	);
};

Button.defaultProps = defaultProps;

export default Button;
