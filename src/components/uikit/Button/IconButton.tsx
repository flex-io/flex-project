import React, { FC } from "react";

interface IconButtonProps extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
	icon?: string;
	ratio?: number;
}
const IconButton: FC<IconButtonProps> = props => {
	return (
		<a
			style={{ display: "flex", justifyContent: "center", alignItems: "center" }}
			uk-icon={`icon: ${props.icon}`.concat(props.ratio ? `;ratio: ${props.ratio}` : ``)}
			{...props}
		>
			{props.children}
		</a>
	);
};

IconButton.defaultProps = {
	icon: "code",
	onClick: e => {
		e.preventDefault();
	}
};

export default IconButton;
