import React, { FC } from "react";

export interface InputProps
	extends React.InputHTMLAttributes<HTMLInputElement> {
	label?: string;
}

const defaultProps: Partial<InputProps> = {
	label: ""
};

const Input: FC<InputProps> = props => {
	return (
		<>
			<label>
				{props.label}
				<input {...props} className="uk-input uk-form-small" type="text" />
			</label>
		</>
	);
};

Input.defaultProps = defaultProps;

export default Input;
