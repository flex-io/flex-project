import React, { PureComponent } from "react";
import IconButton from "components/uikit/Button/IconButton";
import glamorous from "glamorous";
import TextDisplay from "./TextDisplay";

const DatepickerButton = glamorous.a({
	textDecoration: "none",
	color: "gray",
	display: "flex",
	flexDirection: "column",
	flex: 1,
	height: "100%",
	justifyContent: "center",
	":hover, :visited, :focus, :active": {
		textDecoration: "none",
		color: "inherit",
		outline: 0,
		cursor: "pointer"
	}
});

export const AbsolutePosition = glamorous.div({
	position: "absolute",
	right: 20,
	opacity: 0
});

export const SubText = glamorous.div({ fontSize: 10 });
export const Text = glamorous.div({});

export default class CustomDatePicker extends PureComponent<{
	value?: any;
	onClick?: any;
	// onChanged?(value: any): void;
	clearDate?(): void;
	placeholder?: string;
	label?: string;
}> {
	// componentWillReceiveProps(nextProps: any) {
	// 	// if (nextProps.value !== this.props.value) {
	// 	// 	this.props.onChanged(nextProps.value.trim() === "" ? null : nextProps.value);
	// 	// }
	// }
	render() {
		const { value, placeholder, label } = this.props;

		const text = value ? value : placeholder;
		const subText = value ? label : "";
		return (
			<>
				<DatepickerButton onClick={this.props.onClick}>
					<TextDisplay icon="triangle-down" subText={subText}>
						{text}
					</TextDisplay>
				</DatepickerButton>
				{this.props.value && <IconButton icon="close" onClick={this.props.clearDate} />}
			</>
		);
	}
}
