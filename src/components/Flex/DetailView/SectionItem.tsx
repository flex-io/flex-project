import React, { FC, useState, useEffect } from "react";
import glamorous, { StyleArgument, CSSProperties } from "glamorous";
import IconButton from "components/uikit/Button/IconButton";
import Datepicker from "react-datepicker";
import CustomDatePicker from "./CustomDatePicker";
import DropdownMenu, { DropdownMenuProps } from "components/DropdownMenu";
import TextDisplay from "./TextDisplay";
import { moment } from "helper";
import { InputEdit } from "../Forms/InputEdit";
import { InputBase, InputBaseProps } from "../Forms/Input";

interface ISectionItemStyle {
	container: StyleArgument<CSSProperties, undefined>;
	// textContainer: StyleArgument<CSSProperties, undefined>;
	// subText: StyleArgument<CSSProperties, undefined>;
	iconContainer: StyleArgument<CSSProperties, undefined>;
	// rightIconContainer: StyleArgument<CSSProperties, undefined>;
	placeholderText: StyleArgument<CSSProperties, undefined>;
	// dropdownContainer: StyleArgument<CSSProperties, undefined>;
}
export const SectionItemStyle: ISectionItemStyle = {
	container: {
		display: "flex",
		minHeight: 52,
		alignItems: "center",
		paddingRight: 10
	},
	// textContainer: {
	// 	flex: 1,
	// 	display: "flex",
	// 	flexDirection: "column"
	// },
	iconContainer: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		// margin: "0 12px",
		width: 40
	},
	// subText: {
	// 	fontSize: 10
	// },
	placeholderText: {
		color: "gray"
	}
	// dropdownContainer: {
	// 	border: "2px solid #eaeaea",
	// 	top: -20,
	// 	boxShadow: "5px 10px 10px #888888"
	// },
	// rightIconContainer: {
	// 	margin: 8
	// }
};
// const DropdownContainer = glamorous.div(SectionItemStyle.dropdownContainer);
const SectionItemContainer = glamorous.div<{ noHover?: boolean }>(SectionItemStyle.container, props => ({
	":hover": {
		backgroundColor: props.noHover ? "none" : "#eaeaea",
		boxShadow: props.noHover ? "" : "none"
	}
}));
// const TextContainer = glamorous.div(SectionItemStyle.textContainer);
// const SubText = glamorous.div(SectionItemStyle.subText);
const LeftIconContainer = glamorous.div(SectionItemStyle.iconContainer);
// const Text = glamorous.div({});
const InputCheckbox = glamorous.input({ marginTop: 0 });
// const RightIconContainer = glamorous.div<{ hideByDefault?: boolean }>(SectionItemStyle.rightIconContainer, props => ({
// 	opacity: props.hideByDefault ? 0 : 1
// }));

interface ComponentProps {
	Icon?: string | React.ReactNode;
	onRemove?: () => void;
}

interface ItemLinkProps {
	link: string;
	text: string;
	subText?: string;
	onRemove?: () => void;
}

function withSectionItemLink<P extends ComponentProps>(Component: React.ComponentType<P>) {
	const ItemLink: FC<P & ItemLinkProps> = props => {
		return (
			<Component Icon="image" {...(props as P)}>
				<TextDisplay subText={props.subText}>
					<a target="_blank" href={props.link}>
						{props.text}
					</a>
				</TextDisplay>
			</Component>
		);
	};

	return ItemLink;
}

type InternalProps = {
	label?: string;
	placeholder?: string;
	defaultValue?: any;
	onChanged?(value: any): void;
	Icon?: string | React.ReactNode;
};

function withSectionItemDropdownMenu<P extends ComponentProps>(Component: React.ComponentType<P>) {
	const ItemDropdownMenu: FC<P & DropdownMenuProps> = props => {
		return (
			<Component {...(props as P)}>
				<DropdownMenu {...props} />
			</Component>
		);
	};
	return ItemDropdownMenu;
}

function withSectionItemDatePicker<P extends ComponentProps>(Component: React.ComponentType<P>) {
	const DatePick: FC<P & InternalProps> = props => {
		const [DatePicked, setDatePicked] = useState(props.defaultValue ? moment(props.defaultValue).toDate() : null);

		const ClearDate = () => {
			setDatePicked(null);
		};

		return (
			<Component {...(props as P)} Icon={props.Icon}>
				<Datepicker
					selected={DatePicked}
					onChange={() => {}}
					onSelect={date => {
						props.onChanged && props.onChanged(date);
						setDatePicked(date);
					}}
					placeholderText={props.placeholder}
					dateFormat="d MMMM yyyy"
					customInput={<CustomDatePicker label={props.label} clearDate={ClearDate} />}
				/>
			</Component>
		);
	};

	return DatePick;
}

function withSectionItemCheckbox<T extends ComponentProps>(Component: React.ComponentType<T>) {
	type ItemInputProps = {
		text?: string;
		isCheck?: boolean;
		onCheckChanged?: (value: any) => void;
		onTextChanged?: (value: any) => void;
		onRemove?: () => void;
	};
	const RenderCheckbox: FC<React.InputHTMLAttributes<HTMLInputElement>> = props => {
		return <InputCheckbox className="uk-checkbox" type="checkbox" {...props} />;
	};
	const ItemInput: FC<ItemInputProps> = props => {
		return (
			<Component
				{...(props as T)}
				Icon={() => (
					<RenderCheckbox
						checked={props.isCheck}
						onChange={(e: any) => props.onCheckChanged && props.onCheckChanged(e.target.checked)}
					/>
				)}
			>
				<InputEdit onChanged={props.onTextChanged} defaultValue={props.text} />
			</Component>
		);
	};
	ItemInput.displayName = "inputbox";
	return ItemInput;
}

function withSectionItemAddInput<P extends ComponentProps>(Component: React.ComponentType<P>) {
	const ItemAddInput: FC<P & InputBaseProps> = props => {
		return (
			<Component {...(props as P)} Icon="plus">
				<InputBase {...props} blurOnEnter={false} onChanged={props.onChanged} />
			</Component>
		);
	};
	ItemAddInput.displayName = "inputbox";
	return ItemAddInput;
}

function withSectionItemInput<P extends ComponentProps>(Component: React.ComponentType<P>) {
	type ItemInputProps = {
		onChanged?: (value: any) => void;
		defaultValue?: string;
		Icon?: string | React.ReactNode;
	};
	const ItemInput: FC<P & ItemInputProps> = props => {
		return (
			<Component {...(props as P)}>
				<InputEdit {...(props as P)} />
			</Component>
		);
	};
	ItemInput.displayName = "inputbox";
	return ItemInput;
}

function withSectionItemTabs<P extends ComponentProps>(Component: React.ComponentType<P>) {
	type ItemTabsProps = {};
	const ItemTabs: FC<P & ItemTabsProps> = props => {
		return (
			<Component {...(props as P)} Icon="none">
				{props.children}
			</Component>
		);
	};

	return ItemTabs;
}

const NewSectionItem: FC<ComponentProps> = props => {
	const { Icon } = props;
	const [NoHover, setNoHover] = useState(false);
	useEffect(() => {
		React.Children.map(props.children, (child: any) => {
			setNoHover(child.type.name == "InputEdit" || child.type.name == "InputBase");
		});
	}, [NoHover]);

	return (
		<SectionItemContainer noHover={NoHover}>
			<LeftIconContainer>
				{typeof Icon === "function" ? <Icon /> : <span uk-icon={`icon: ${props.Icon}`} />}
			</LeftIconContainer>
			<div style={{ flex: 1, display: "flex" }}>{props.children}</div>
			{props.onRemove && <IconButton icon="trash" onClick={props.onRemove} />}
		</SectionItemContainer>
	);
};

NewSectionItem.defaultProps = {
	Icon: "file-text"
};

export const SectionItemInput = withSectionItemInput(NewSectionItem);
export const SectionItemCheckbox = withSectionItemCheckbox(NewSectionItem);
export const SectionItemDatePicker2 = withSectionItemDatePicker(NewSectionItem);
export const SectionItemDropdownMenu = withSectionItemDropdownMenu(NewSectionItem);
export const SectionItemLinkAnchor = withSectionItemLink(NewSectionItem);
export const SectionItemTabs = withSectionItemTabs(NewSectionItem);
export const SectionItemAddInput = withSectionItemAddInput(NewSectionItem);
