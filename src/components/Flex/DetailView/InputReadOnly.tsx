import React, { FC, useState } from "react";
import glamorous from "glamorous";

interface InputReadOnlyProps extends React.InputHTMLAttributes<HTMLInputElement> {
	isEditable?: boolean;
	onChanged?(value: any): void;
}

export const InputReadOnly: FC<InputReadOnlyProps> = props => {
	const [inputReadOnly, setInputReadOnly] = useState(true);
	const _InputReadOnly = glamorous.input<{ isEdit?: boolean }>({}, props => ({
		border: !props.isEdit ? "none" : ""
	}));
	const [value, setValue] = useState(props.defaultValue);

	function onChangeEvent(e: any) {
		
		setValue(e.target.value);
	}

	return (
		<_InputReadOnly
			className="uk-input"
			type="text"
			autoFocus
			isEdit={!inputReadOnly}
			value={value}
			readOnly={!props.isEditable || inputReadOnly}
			onBlur={() => setInputReadOnly(true)}
			onDoubleClick={() => {
				setInputReadOnly(false);
			}}
			onChange={onChangeEvent}
			onKeyDown={(e: any) => {
				if (e.key === "Enter") {
					setValue(e.target.value);
					props.onChanged && props.onChanged(value);
					e.target.blur();
				} else if (e.key === "Escape") {
					setValue(value);
					e.target.blur();
				}
			}}
		/>
	);
};

InputReadOnly.defaultProps = { isEditable: true };
