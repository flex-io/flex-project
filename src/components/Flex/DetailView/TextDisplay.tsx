import React, { FC } from "react";
import glamorous from "glamorous";

const Container = glamorous.div({ display: "flex", justifyContent: "center", alignItems: "center", paddingLeft: 10 });
const TextContainer = glamorous.div({ display: "flex", flexDirection: "column", flex: 1 });
const SubText = glamorous.div({ fontSize: 10 });
const Text = glamorous.div({});

type TextDisplayProps = {
	subText?: string;
	icon?: string;
};

const TextDisplay: FC<TextDisplayProps> = props => {
	return (
		<>
			<Container>
				<TextContainer>
					<Text>{props.children}</Text>
					<SubText>{props.subText}</SubText>
				</TextContainer>
				{props.icon && <span uk-icon={`icon: ${props.icon}`} />}
			</Container>
		</>
	);
};


export default TextDisplay;
