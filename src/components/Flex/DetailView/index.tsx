import React, { FC, useState } from "react";
import glamorous, { StyleArgument, CSSProperties } from "glamorous";
import { ToggleIcon } from "../../../app/pages/Projects/Components/ToggleIcon";

interface IStyle {
	main: StyleArgument<CSSProperties, undefined>;
	header: StyleArgument<CSSProperties, undefined>;
	body: StyleArgument<CSSProperties, undefined>;
	footer: StyleArgument<CSSProperties, undefined>;
	section: StyleArgument<CSSProperties, undefined>;
	sectionHeader: StyleArgument<CSSProperties, undefined>;
	sectionBody: StyleArgument<CSSProperties, undefined>;
}

export const DetailStyle: IStyle = {
	main: {
		width: 450,
		maxWidth: 450,
		flex: "1 1 auto",
		display: "flex",
		flexDirection: "column",
		borderLeft: "1px solid #eaeaea",
		backgroundColor: "#F3F2F1"
	},
	header: {
		display: "flex",
		justifyContent: "space-between"
	},
	body: {
		flex: "1 1 auto",
		overflow: "auto"
	},
	footer: {
		// minHeight: 60,
		display: "flex",
		// justifyContent: "center",
		alignItems: "center",
		padding: "5px 15px",
		borderTop: "1px solid rgba(0, 0, 0, 0.08)"
	},
	section: {
		flex: 1,
		padding: 10
	},
	sectionHeader: {
		display: "flex",
		justifyContent: "space-between",
		padding: "10px 5px 0 0"
	},
	sectionBody: {
		display: "flex",
		flexDirection: "column",
		backgroundColor: "white",
		// overflow: "auto",
		// "> div": {
		// 	boxShadow: "24px 25px 0 -24px rgba(0, 0, 0, 0.08)"
		// },
		"> div:hover": {
			// backgroundColor: "#eaeaea",
			// boxShadow: "none",
			"> div > div > div": {
				opacity: 1
			}
		},
		"> div:last-child": {
			boxShadow: "none"
		}
	}
};

//SECTION
interface SectionProps {
	expand?: boolean;
	icon?: any;
	header?: string;
	collapsible?: boolean;
	noHover?: boolean;
	noDivider?: boolean;
}

const Section: FC<SectionProps> = props => {
	const [Expanded, setExpanded] = useState(props.expand);
	const SectionBody = glamorous.div<{ noDivider?: boolean }>(DetailStyle.sectionBody, props => ({
		// "> div:hover": {
		// 	backgroundColor: props.noHover ? "none" : "#eaeaea",
		// 	boxShadow: props.noHover ? "" : "none"
		// },
		"> div": {
			boxShadow: props.noDivider ? "none" : "24px 25px 0 -24px rgba(0, 0, 0, 0.08)"
		}
	}));
	const Section = glamorous.div(DetailStyle.section);
	const SectionHeader = glamorous.div(DetailStyle.sectionHeader);

	return (
		<Section>
			<SectionHeader>
				{props.header}
				{props.collapsible && <ToggleIcon toggled={Expanded} onToggle={() => setExpanded(!Expanded)} />}
			</SectionHeader>
			{props.collapsible ? (
				Expanded && <SectionBody>{props.children}</SectionBody>
			) : (
				<SectionBody>{props.children}</SectionBody>
			)}
		</Section>
	);
};
// {
// 	props.collapsible
// 		? Expanded &&
// 		  React.Children.map(props.children, (child: any) => {
// 				return (
// 					<SectionBody
// 						noDivider={false}
// 						noHover={child.type.displayName === "checkbox" || child.type.displayName === "inputbox"}
// 					>
// 						{child}
// 					</SectionBody>
// 				);
// 		  })
// 		: React.Children.map(props.children, (child: any) => {
// 				return (
// 					<SectionBody
// 						noDivider={false}
// 						noHover={child.type.displayName === "checkbox" || child.type.displayName === "inputbox"}
// 					>
// 						{child}
// 					</SectionBody>
// 				);
// 		  });
// }
Section.defaultProps = { expand: false, collapsible: false, noHover: false, noDivider: false };

const FooterContainer = glamorous.div(DetailStyle.footer);
export class DetailFooter extends React.PureComponent<{}> {
	static displayName = "DetailFooter";
	static LeftPane: FC<{}> = props => {
		return <div>{props.children}</div>;
	};
	static CenterPane: FC<{}> = props => {
		return <div style={{ flex: "1 1 auto", display: "flex", justifyContent: "center" }}>{props.children}</div>;
	};
	static RightPane: FC<{}> = props => {
		return <div>{props.children}</div>;
	};
	render() {
		return <FooterContainer>{this.props.children}</FooterContainer>;
	}
}

////////////

const DetailViewContainer = glamorous.div(DetailStyle.main);

export default class DetailView extends React.PureComponent<{}> {
	static Header: FC<{}> = props => {
		const Container = glamorous.div(DetailStyle.header);

		return <Container>{props.children}</Container>;
	};

	static Section = Section;

	static Body: FC<{ noHover?: boolean }> = props => {
		const Container = glamorous.div(DetailStyle.body);
		return <Container>{props.children}</Container>;
	};

	render() {
		return <DetailViewContainer>{this.props.children}</DetailViewContainer>;
	}
}
