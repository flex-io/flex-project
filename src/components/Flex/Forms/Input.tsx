import React, { FC } from "react";
import glamorous, { ExtraGlamorousProps } from "glamorous";

const Input = glamorous.input({
	fontSize: "inherit",
	color: "inherit",
	border: "none !important",
	outline: "none",
	width: "100%",
	height: "100%",
	// margin: "5px 5px",
	fontFamily: "inherit",
	"::placeholder": {
		color: "#0F6DCD"
	},
	":focus": {
		"::placeholder": {
			color: "#9A9A9A"
		}
	}
});

export type InputBaseProps = {
	onChanged?: (value: any) => void;
	defaultValue?: any;
	placeholder?: any;
	readOnly?: boolean;
	blurOnEnter?: boolean;
} & ExtraGlamorousProps;

export const InputBase: FC<InputBaseProps> = props => {


	return (
		<Input
			{...props}
			innerRef={props.innerRef}
			onBlur={e => {
				let val = props.defaultValue ? props.defaultValue : null;
				e.target.value = val;
			}}
			onKeyDown={(e: any) => {
				if (e.key === "Enter") {
					props.onChanged && props.onChanged(e.target.value);
					e.target.value = props.defaultValue ? props.defaultValue : null;
					props.blurOnEnter && e.target.blur();
				} else if (e.key === "Escape") {
					let val = props.defaultValue ? props.defaultValue : null;
					e.target.value = val;
					e.target.blur();
				}
			}}
		/>
	);
};

InputBase.defaultProps = { blurOnEnter: true };

// function withIcon<P extends React.InputHTMLAttributes<HTMLInputElement>>(Component: React.ComponentType<P>) {
// 	const IconComponent: FC<P & React.InputHTMLAttributes<HTMLInputElement>> = props => {
// 		return (
// 			<>
// 				<IconButton icon="plus" />
// 				<Component {...(props as P)} />
// 			</>
// 		);
// 	};

// 	return IconComponent;
// }

// export const IconInput = withIcon(InputEdit);
