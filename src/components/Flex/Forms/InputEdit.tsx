import glamorous from "glamorous";
import React, { FC, useState, useRef, useEffect } from "react";
import { InputBase } from "./Input";
import IconButton from "components/uikit/Button/IconButton";

const InputContainer = glamorous.div<{ isReadOnly?: boolean }>(
	{
		display: "flex",
		alignItems: "center",
		width: "100%",
		padding: "3px"
	},
	props => ({ border: props.isReadOnly ? "none" : `1px solid #666` })
);
const IconButtonContainer = glamorous.div({ opacity: 0 });

export type InputEditProps = {
	isEditable?: boolean;
	onChanged?: (value: any) => void;
};

export const InputEdit: FC<InputEditProps & React.InputHTMLAttributes<HTMLInputElement>> = props => {
	const [inputReadOnly, setInputReadOnly] = useState(true);
	const inputRef = useRef(null);
	const node = useRef();
	// function onChangeEvent(e: any) {
	// 	setValue(e.target.value);
	// }
	useEffect(() => {
		document.addEventListener("mousedown", handleClick);

		return () => {
			document.removeEventListener("mousedown", handleClick);
		};
	}, [inputReadOnly]);
	const handleClick = (e: any) => {
		//@ts-ignore
		if (node.current.contains(e.target)) {
			// inside click
			return;
		}

		setInputReadOnly(true);
		// outside click
		// ... do whatever on click outside here ...
	};
	return (
		<InputContainer isReadOnly={inputReadOnly} innerRef={node}>
			<InputBase
				// onChange={onChangeEvent}
				{...props}
				readOnly={!props.isEditable || inputReadOnly}
				innerRef={inputRef}
				onChanged={value => {
					console.log(value);
					setInputReadOnly(true);
					props.onChanged && props.onChanged(value);
				}}
				// onBlur={() => setInputReadOnly(true)}
				// onKeyDown={(e: any) => {
				// 	if (e.key === "Enter") {
				// 		setValue(e.target.value);
				// 		props.onChanged && props.onChanged(value);
				// 		setInputReadOnly(true);
				// 		e.target.blur();
				// 	} else if (e.key === "Escape") {
				// 		setValue(value);
				// 		setInputReadOnly(true);
				// 		e.target.blur();
				// 	}
				// }}
			/>
			{inputReadOnly ? (
				<IconButtonContainer>
					<IconButton
						icon="pencil"
						onClick={() => {
							inputRef.current.focus();
							setInputReadOnly(false);
						}}
					/>
				</IconButtonContainer>
			) : (
				<IconButton icon="check" onClick={() => setInputReadOnly(true)} />
			)}
		</InputContainer>
	);
};

InputEdit.defaultProps = { isEditable: true };
