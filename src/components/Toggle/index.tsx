import React, { FC, useState } from "react";
import { Button } from "components/uikit";

interface ToggleProps {
	toggleOnText: string;
	toggleOffText: string;
	isToggle: boolean;
	onChanged?(val: boolean): void;
}

export const ToggleText: FC<ToggleProps> = props => {
	const [isToggle, toggle] = useState(props.isToggle);
	const toggleText = isToggle ? props.toggleOnText : props.toggleOffText;
	
	function onClicked(e: any) {
		e.preventDefault();
		toggle(!isToggle);
		props.onChanged(!isToggle);
	}

	return (
		<Button button_type="link" onClick={onClicked}>
			{toggleText}
		</Button>
	);
};
