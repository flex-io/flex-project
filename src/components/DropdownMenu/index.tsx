import React, { FC, useEffect, useRef, useState } from "react";
import glamorous from "glamorous";
import TextDisplay from "components/Flex/DetailView/TextDisplay";
import IconButton from "components/uikit/Button/IconButton";

const Container = glamorous.div({
	cursor: "pointer",
	width: "100%",
	height: "52px",
	display: "flex",
	flexDirection: "column",
	position: "relative"
});

const DropdownContainer = glamorous.div(
	{
		zIndex: 1000,
		position: "absolute",
		boxSizing: "border-box",
		width: "100%",
		backgroundColor: "white",
		border: "1px solid rgba(0, 0, 0, 0.08)",
		boxShadow: "5px 10px 8px #eaeaea"
	},
	({ theme }: any) => ({ boxShadow: `5px 10px 8px ${theme.colors.borderColor}` })
);

const DropdownButton = glamorous.a({
	textDecoration: "none",
	color: "gray",
	display: "flex",
	flexDirection: "column",
	flex: 1,
	height: "100%",
	justifyContent: "center",
	":hover, :visited, :focus, :active": {
		textDecoration: "none",
		color: "inherit",
		outline: 0,
		cursor: "pointer"
	}
});

const MenuItem = glamorous.a({
	// backgroundColor: "#eee",
	color: "black",
	display: "block",
	padding: "12px",
	textDecoration: "none",
	":hover": {
		backgroundColor: "#ccc"
	},
	":active": {
		backgroundColor: "#4CAF50",
		color: "white"
	}
});

type ListItemType = {
	id: any;
	text: string;
};

export type DropdownMenuProps = {
	list: ListItemType[];
	onChanged?: (id: string) => void;
	subText?: string;
	placeholder?: string;
	defaultValue?: any;
};

const DropdownMenu: FC<DropdownMenuProps> = props => {
	const node = useRef();
	const [isOpen, openPopper] = useState(false);
	const [Selected, setSelected] = useState<string>(props.defaultValue);

	// const [Selected, setSelected] = useState<ListItemType>(props.list.find(list => list.id === props.defaultValue));

	useEffect(() => {
		document.addEventListener("mousedown", handleClick);

		return () => {
			document.removeEventListener("mousedown", handleClick);
		};
	}, [isOpen]);
	const handleClick = (e: any) => {
		//@ts-ignore
		if (node.current.contains(e.target)) {
			// inside click
			return;
		}

		openPopper(false);
	};
	function handleChange(id: string) {
		setSelected(id);
		if (props.onChanged) props.onChanged(id);
		if (id) openPopper(!isOpen);
	}
	return (
		<>
			<Container innerRef={node}>
				<DropdownButton onClick={() => openPopper(!isOpen)}>
					<TextDisplay subText={Selected ? props.subText : ""}>
						{Selected ? props.list.find(list => list.id === Selected).text : props.placeholder}
					</TextDisplay>
				</DropdownButton>

				{isOpen && (
					<div style={{ position: "relative" }}>
						<DropdownContainer>
							{props.list.map(item => {
								return (
									<MenuItem key={item.id} onClick={() => handleChange(item.id)}>
										{item.text}
									</MenuItem>
								);
							})}
						</DropdownContainer>
					</div>
				)}
			</Container>
			{Selected && <IconButton icon="close" onClick={() => handleChange(null)} />}
		</>
	);
};

DropdownMenu.defaultProps = {
	list: [{ id: "123", text: "Test" }, { id: "1234", text: "Test 2" }]
};

export default DropdownMenu;
