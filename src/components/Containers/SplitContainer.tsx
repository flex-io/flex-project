import React from "react";

export interface InjectedProps {
	S1: React.ComponentType<React.HtmlHTMLAttributes<HTMLDivElement>>;
	S2: React.ComponentType<React.HtmlHTMLAttributes<HTMLDivElement>>;
}

interface IProps {
	children(props: InjectedProps): JSX.Element;
}

class SplitContainer extends React.Component<IProps> {
	S1 = (props?: any) => {
		return <div className={"s1 ".concat(props.className)}>{props.children}</div>;
	};

	S2 = (props?: any) => {
		return (
			<div className={"s2 ".concat(props.className)}>
				{props.children}
			</div>
		);
	};

	render() {
		return (
			<div className="split-container split-container-row">
				{this.props.children({
					S1: this.S1.bind(this),
					S2: this.S2.bind(this)
				})}
			</div>
		);
	}
}

export default SplitContainer;
