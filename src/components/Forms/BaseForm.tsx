import React from "react";
import _ from "lodash";

export type FormComponentProps<FormFields> = {
	fields: FormFields;
} & Handler<FormFields>


interface Props<FormFields> {
	FormComponent: React.ComponentType<FormComponentProps<FormFields>>;
	// children(props: FormComponentProps<FormFields>): React.ComponentType;
	initialValue: FormFields;
}

interface State<FormFields> {
	fields: FormFields;
}

type OnChangeHandler<FormFields> = <K extends keyof FormFields>(
	s: K,
	a: FormFields[K]
) => void;

interface Handler<FormFields> {
	onChange: OnChangeHandler<FormFields>;
}

export default class BaseForm<FormFields> extends React.Component<
	Props<FormFields>,
	State<FormFields>
> {
	constructor(props: Props<FormFields>) {
		super(props);

		this.state = {
			fields: this.props.initialValue
		};
	}

	onChange: OnChangeHandler<FormFields> = (field, value) => {
		const fields: any = _.assign({}, this.state.fields, { [field]: value });

		this.setState({ ...this.state, fields });
	};

	FormInput(props: React.InputHTMLAttributes<HTMLInputElement>) {
		return <input {...props} />;
	}
	render() {
		const { FormComponent } = this.props;
		const { fields } = this.state;

		return (
			<>
				<FormComponent
					onChange={this.onChange}
					fields={fields}
				/>
			</>
		);
	}
}
