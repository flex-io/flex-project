import React, { PureComponent } from "react";
const uuid = require("uuid");

const Cell: React.FC<{
	content: any;
	header?: any;
}> = props => {
	const cellMarkup = props.header ? (
		<th>{props.content}</th>
	) : (
		<td>{props.content}</td>
	);

	return cellMarkup;
};

export default class index extends PureComponent<{
	headings: any[];
	rows: any[];
}> {
	renderHeadingRow: React.FC<{ key: any; title: any }> = _cell => {
		return (
			<Cell
				key={`heading-${_cell.key}`}
				content={_cell.title}
				header="true"
			/>
		);
	};

	renderRow = (_row: any[]) => {
		const { headings } = this.props;
		const filteredHeadings = headings.filter(header => {
			return header.visible !== false;
		});
		return _row.map(row => {
			return (
				<tr key={`${row.id}`}>
					{filteredHeadings.map(header => {
						return (
							<td key={uuid()}>
								{header.render
									? header.render(row)
									: row[header.dataIndex]}
							</td>
						);
					})}
				</tr>
			);
		});
	};

	render() {
		const { headings, rows } = this.props;
		const filteredHeadings = headings.filter(header => {
			return header.visible !== false;
		});

		// const sortedRows = rows[0].sort((a: any, b: any) => {
		// 	return a.dateTarget < b.dateTarget;
		// });

		this.renderHeadingRow = this.renderHeadingRow.bind(this);
		this.renderRow = this.renderRow.bind(this);

		const theadMarkup = (
			<tr key="heading">{filteredHeadings.map(this.renderHeadingRow)}</tr>
		);

		const tbodyMarkup = [rows].map(this.renderRow);

		return (
			<div>
				<table className="uk-table uk-table-small uk-table-divider">
					<thead>{theadMarkup}</thead>
					<tbody>{tbodyMarkup}</tbody>
				</table>
			</div>
		);
	}
}
