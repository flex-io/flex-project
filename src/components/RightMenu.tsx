import React from "react";
import { Link } from "react-router-dom";
import { IView } from "app";
import { shortid } from "helper";
import { Auth } from "aws-amplify";

const RightMenu = (props: { views: IView[] }) => {
	return (
		<div className="uk-navbar-right">
			<ul className="uk-navbar-nav uk-visible@s">
				{props.views
					.filter(view => {
						return view.isSub !== false;
					})
					.map(view => {
						return (
							<li key={shortid.generate()} className="router-link-exact-active uk-active">
								<Link to={view.path}>{view.name}</Link>
							</li>
						);
					})}
				<li className="router-link-exact-active uk-active">
					<Link to="/" onClick={() => Auth.signOut()}>Logout</Link>
				</li>
			</ul>

			<a uk-navbar-toggle-icon="" href="#offcanvas-push" uk-toggle="" className="uk-navbar-toggle uk-hidden@s" />
		</div>
	);
};

export default RightMenu;
