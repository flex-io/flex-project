import * as React from "react";

interface InjectedCounterProps {
	value: number;
	onIncrement(): void;
	onDecrement(): void;
}

interface MakeCounterProps {
	minValue?: number;
	maxValue?: number;
	children(props: InjectedCounterProps): JSX.Element;
}

interface MakeCounterState {
	value: number;
}

export default class MakeCounter extends React.Component<
	MakeCounterProps,
	MakeCounterState
> {
	constructor(props: MakeCounterProps) {
		super(props);

		this.state = {
			value: 0
		};
	}

	increment = () => {
		this.setState(prevState => ({
			value:
				prevState.value === this.props.maxValue
					? prevState.value
					: prevState.value + 1
		}));
	};

	decrement = () => {
		this.setState(prevState => ({
			value:
				prevState.value === this.props.minValue
					? prevState.value
					: prevState.value - 1
		}));
	};

	render() {
		return this.props.children({
			value: this.state.value,
			onIncrement: this.increment.bind(this),
			onDecrement: this.decrement.bind(this)
		});
	}
}
