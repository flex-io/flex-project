import React, { FC, useState } from "react";
import { Button } from "components/uikit";


interface ModalProps {
	text: string;
	onClickClose?(): void;
	onClickOk?(): void;
	okText?: string;
	closeText?: string;
}

const defaultProps: Partial<ModalProps> = {
	okText: "Create",
	closeText: "Close"
};

export const Modal: FC<ModalProps> = props => {
	const [isOpen, show] = useState(false);

	function onClickClose(e: any) {
		e.preventDefault();
		props.onClickClose && props.onClickClose();
		show(false);
	}

	function onClickOk(e: any) {
		e.preventDefault();
		props.onClickOk && props.onClickOk();
		show(false);
	}

	return (
		<>
			<Button
				button_type="link"
				onClick={e => {
					e.preventDefault();
					show(true);
				}}
			>
				{props.text}
			</Button>
			{isOpen && (
				<div className="modal">
					<section className="modal-main">
						<div className="modal-header" />
						<div className="modal-content">{props.children}</div>
						<div className="modal-footer">
							<Button button_type="link" onClick={onClickClose}>
								{props.closeText}
							</Button>
							<Button onClick={onClickOk}>{props.okText}</Button>
						</div>
					</section>
				</div>
			)}
		</>
	);
};

Modal.defaultProps = defaultProps;
